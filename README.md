# Zee5iOSPlayer

[![CI Status](https://img.shields.io/travis/Manibaratam/Zee5iOSPlayer.svg?style=flat)](https://travis-ci.org/Manibaratam/Zee5iOSPlayer)
[![Version](https://img.shields.io/cocoapods/v/Zee5iOSPlayer.svg?style=flat)](https://cocoapods.org/pods/Zee5iOSPlayer)
[![License](https://img.shields.io/cocoapods/l/Zee5iOSPlayer.svg?style=flat)](https://cocoapods.org/pods/Zee5iOSPlayer)
[![Platform](https://img.shields.io/cocoapods/p/Zee5iOSPlayer.svg?style=flat)](https://cocoapods.org/pods/Zee5iOSPlayer)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

Zee5iOSPlayer is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'Zee5iOSPlayer'
```

## Author

Manibaratam, manikantabaratam94@gmail.com

## License

Zee5iOSPlayer is available under the MIT license. See the LICENSE file for more info.
