Pod::Spec.new do |s|
  
  s.name             = 'Zee5iOSPlayer'
  s.version          = '2.0.0'
  s.summary          = 'An Example of full screen plugin for Zapp iOS.'
  s.description      = 'Hello ksjafjg aksjfasfhaskfhgashfahsfhkasfkash flkhas flhk hksafh'
  s.homepage         = 'https://manibaratam@bitbucket.org/manibaratam/zee5iosplayer.git'
  s.license          = 'MIT'
  s.author           = { 'manibaratam' => 'manikanta.baratam@zee.esselgroup.com' }
  s.source           = { :git => 'https://manibaratam@bitbucket.org/manibaratam/zee5iosplayer.git', :tag => s.version.to_s }
  
  s.ios.deployment_target  = '9.0'
  s.platform     = :ios, '9.0'
  s.requires_arc = true
  s.swift_version = '4.2'
#  s.source_files = 'PluginClasses/*.{swift,h,m}'

  s.subspec 'Core' do |c|
    c.resources = 'Resources/**/*'
    c.frameworks = 'UIKit'
    c.source_files = 'PluginClasses/**/*'
    c.public_header_files = 'PluginClasses/PlayerManager/ZEE5PlayerManager.h' 
    c.dependency 'ZappGeneralPluginsSDK'
    c.dependency 'ZappPlugins'
    c.dependency 'Alamofire'
    c.dependency 'ApplicasterSDK'
    c.dependency 'JWPlayer-SDK'
    c.dependency 'FBAudienceNetwork' 
    c.dependency 'GoogleAds-IMA-iOS-SDK'


  end

  s.xcconfig =  { 'CLANG_ALLOW_NON_MODULAR_INCLUDES_IN_FRAMEWORK_MODULES' => 'YES',
    'ENABLE_BITCODE' => 'YES',
    'SWIFT_VERSION' => '4.2'
  }
  
  s.default_subspec = 'Core'
  
end

