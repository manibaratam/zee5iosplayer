//
//  CurrentItem.h
//  ZEE5PlayerSDK
//
//  Created by admin on 06/12/18.
//  Copyright © 2018 ZEE5. All rights reserved.
//

typedef enum {
    vod,
    live,
    dvr
} StreamType;

typedef enum playerType
{
    normalPlayer,
    mutedPlayer,
    unmutedPlayer
} PlayerType;


#import <Foundation/Foundation.h>
#import "Genres.h"
#import "RelatedVideos.h"
#import "ZEE5AdModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface CurrentItem : NSObject
@property(strong, nonnull) NSString *drm_key;
@property(strong, nonnull) NSString *hls_Url;
@property(strong, nonnull) NSString *hls_Full_Url;
@property (nonatomic, copy)   NSString *mpd_Url;

@property(strong, nonnull) NSString *drm_token;
@property(strong, nonnull) NSString *business_type;
@property(strong, nonnull) NSArray *subTitles;
@property (nonatomic)StreamType streamType;
@property (strong, nonatomic) NSString* asset_type;
@property (strong, nonatomic) NSString* asset_subtype;
@property (nonatomic)   NSInteger episode_number;
@property (nonatomic, copy)   NSString *release_date;

@property (strong, nonatomic) NSArray<Genres*> *geners;
@property (strong, nonatomic) NSArray<RelatedVideos*> *related;
@property (strong, nonatomic) NSMutableArray<ZEE5AdModel*> *fanAds;
@property (strong, nonatomic) NSArray<ZEE5AdModel*> *googleAds;

@property(strong, nonnull) NSString *content_id;
@property(strong, nonnull) NSString *channel_Name;
@property(strong, nonnull) NSString *showName;
@property (nonatomic, readwrite)  BOOL isDRM;
@property (nonatomic, copy)   NSDictionary *skipAvailable;
@property (nonatomic, copy)   NSString *endCreditsMarker;

@end

NS_ASSUME_NONNULL_END
