//
//  ZEE5PlayerManager.m
//  ZEE5PlayerSDK
//
//  Created by admin on 04/12/18.
//  Copyright © 2018 ZEE5. All rights reserved.
//



#import "ZEE5PlayerManager.h"
#import <AVFoundation/AVFoundation.h>
#import "EpgContentDetailsDataModel.h"
#import "VODContentDetailsDataModel.h"
#import <AdSupport/ASIdentifierManager.h>
#import "CustomControl.h"
#import "Zee5MuteView.h"
#import "ParentalControl.h"
#import "Zee5MenuView.h"
#import "Zee5FanAdManager.h"
#import "ZEE5UserDefaults.h"
#import "ReportingManager.h"
#import "BaseUrls.h"
//#import "ZEE5PlayerSDK/ZEE5PlayerSDK-Swift.h"
//#import <Zee5iOSPlayer/ZEE5PlayerSDK-Swift.h>


#import "ZEE5AudioTrack.h"
#import "ZEE5Subtitle.h"
#import "Genres.h"
#import "ZEE5AdModel.h"
#import "SimilarDataModel.h"
#import <PlayKit/PlayKit-Swift.h>
#import <PlayKit_IMA/PlayKit_IMA-Swift.h>
#import "Zee5PlayerPlugin.h"
#import "SubscriptionModel.h"
#define HIDECONTROLSVALUE 5.0
#define TOPBARHEIGHT 30

#define SUBTITLES @"Subtitle"
#define LANGUAGE @"Audio Track"
#define VIDEOQUALITY @"Quality"
#define AUTOPLAY @"Enable auto Play"
#define WATCHLIST @"Add to watch List"

#define POSTTIME @"post"


#define TOKEN @"eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYmYiOjE1NjEzNzEzMDUsImV4cCI6MTU5MjkwNzMwNSwiaXNzIjoiaHR0cHM6Ly91c2VyYXBpLnplZTUuY29tIiwiYXVkIjpbImh0dHBzOi8vdXNlcmFwaS56ZWU1LmNvbS9yZXNvdXJjZXMiLCJzdWJzY3JpcHRpb25hcGkiLCJ1c2VyYXBpIl0sImNsaWVudF9pZCI6InRva2VuX2NsaWVudCIsInN1YiI6ImZiMjBhNmEzLTRiMDktNDBiOS05MDBhLTBjMTkxMTc5OTlkNCIsImF1dGhfdGltZSI6MTU2MTM3MTMwNSwiaWRwIjoibG9jYWwiLCJ1c2VyX2lkIjoiZmIyMGE2YTMtNGIwOS00MGI5LTkwMGEtMGMxOTExNzk5OWQ0Iiwic3lzdGVtIjoiWjUiLCJhY3RpdmF0aW9uX2RhdGUiOiIyMDE4LTExLTMwVDIwOjI4OjU3IiwiY3JlYXRlZF9kYXRlIjoiIiwicmVnaXN0cmF0aW9uX2NvdW50cnkiOiJJTiIsInVzZXJfZW1haWwiOiJNYW5pa2FudGEuYmFyYXRhbUB6ZWUuZXNzZWxncm91cC5jb20iLCJ1c2VyX21vYmlsZV9ub3RfdmVyaWZpZWQiOiIxMjM0NTY3ODkwMyIsInN1YnNjcmlwdGlvbnMiOiJbe1wiaWRcIjpcImE2MmYzMmFiLWNjNGEtNDM4YS04ZmRlLTIxMGQ2ODMzMTU3YVwiLFwidXNlcl9pZFwiOlwiZmIyMGE2YTMtNGIwOS00MGI5LTkwMGEtMGMxOTExNzk5OWQ0XCIsXCJpZGVudGlmaWVyXCI6XCJDUk1cIixcInN1YnNjcmlwdGlvbl9wbGFuXCI6e1wiaWRcIjpcIjAtMTEtNjM5XCIsXCJhc3NldF90eXBlXCI6MTEsXCJzdWJzY3JpcHRpb25fcGxhbl90eXBlXCI6XCJTVk9EXCIsXCJ0aXRsZVwiOlwiQWxsIEFjY2VzcyBQYWNrXCIsXCJvcmlnaW5hbF90aXRsZVwiOlwiQWxsIEFjY2VzcyBQYWNrXCIsXCJzeXN0ZW1cIjpcIlo1XCIsXCJkZXNjcmlwdGlvblwiOlwiQWxsIEFjY2VzcyBQYWNrIC0gMTIgTW9udGhzXCIsXCJiaWxsaW5nX2N5Y2xlX3R5cGVcIjpcImRheXNcIixcImJpbGxpbmdfZnJlcXVlbmN5XCI6MzY1LFwicHJpY2VcIjo5OTkuMCxcImN1cnJlbmN5XCI6XCJJTlJcIixcImNvdW50cnlcIjpcIklOXCIsXCJjb3VudHJpZXNcIjpbXCJJTlwiXSxcInN0YXJ0XCI6XCIyMDE4LTEwLTMxVDAwOjAwOjAwWlwiLFwiZW5kXCI6XCIyMDIwLTEyLTMxVDIzOjU5OjU5WlwiLFwib25seV9hdmFpbGFibGVfd2l0aF9wcm9tb3Rpb25cIjp0cnVlLFwicmVjdXJyaW5nXCI6ZmFsc2UsXCJwYXltZW50X3Byb3ZpZGVyc1wiOlt7XCJuYW1lXCI6XCJQYXlUTVwifSx7XCJuYW1lXCI6XCJHb29nbGVcIixcInByb2R1Y3RfcmVmZXJlbmNlXCI6XCJ6ZWU1XzEybV9zdm9kX25yX2luXCJ9LHtcIm5hbWVcIjpcIlBheXRtUVJcIn0se1wibmFtZVwiOlwiUGF5VVwifSx7XCJuYW1lXCI6XCJaRUU1XCJ9LHtcIm5hbWVcIjpcIk1vYmlrd2lrXCJ9LHtcIm5hbWVcIjpcIkFtYXpvbnBheV9uZXdcIn0se1wibmFtZVwiOlwiUXdpa2NpbHZlclwifSx7XCJuYW1lXCI6XCJCaWxsRGVza1wifSx7XCJuYW1lXCI6XCJBbWF6b25JQVBcIixcInByb2R1Y3RfcmVmZXJlbmNlXCI6XCJ6ZWU1X2FsbF9hY2Nlc3NfcGFja19pbl8xeVwifSx7XCJuYW1lXCI6XCJQaG9uZXBlXCJ9XSxcInByb21vdGlvbnNcIjpbe1widGl0bGVcIjpcIlpFRTVNRUQzMFwiLFwiY29kZVwiOlwiWkVFNU1FRDMwXCIsXCJzdGFydF9kYXRlXCI6XCIyMDE5LTA1LTE5VDAwOjAwOjAwWlwiLFwiZW5kX2RhdGVcIjpcIjIwMjAtMDQtMDFUMjM6NTk6NTkuOTk5WlwiLFwiZGlzY291bnRcIjozMDAuMCxcImRpc2NvdW50X3R5cGVcIjpcImN1cnJlbmN5XCIsXCJudW1iZXJfYmlsbGluZ19jeWNsZXNcIjoxfSx7XCJ0aXRsZVwiOlwiR1JBQlpFRVwiLFwiY29kZVwiOlwiR1JBQlpFRVwiLFwic3RhcnRfZGF0ZVwiOlwiMjAxOS0wMS0yNFQxODozODowMFpcIixcImVuZF9kYXRlXCI6XCIyMDE5LTA1LTMxVDE4OjI5OjU5Ljk5OVpcIixcImRpc2NvdW50XCI6MTk5LjgsXCJkaXNjb3VudF90eXBlXCI6XCJjdXJyZW5jeVwiLFwibnVtYmVyX2JpbGxpbmdfY3ljbGVzXCI6MX0se1widGl0bGVcIjpcIlpPT00zNVwiLFwiY29kZVwiOlwiWk9PTTM1XCIsXCJzdGFydF9kYXRlXCI6XCIyMDE5LTA1LTI4VDAwOjAwOjAwWlwiLFwiZW5kX2RhdGVcIjpcIjIwMjAtMDEtMDFUMjM6NTk6NTkuOTk5WlwiLFwiZGlzY291bnRcIjozNTAuMCxcImRpc2NvdW50X3R5cGVcIjpcImN1cnJlbmN5XCIsXCJudW1iZXJfYmlsbGluZ19jeWNsZXNcIjoxfSx7XCJ0aXRsZVwiOlwiWkVFNVNTUExcIixcImNvZGVcIjpcIlpFRTVTU1BMXCIsXCJzdGFydF9kYXRlXCI6XCIyMDE5LTAyLTA1VDE4OjM4OjAwWlwiLFwiZW5kX2RhdGVcIjpcIjIwMjAtMDQtMDJUMTg6Mjk6NTkuOTk5WlwiLFwiZGlzY291bnRcIjoyMDAuMCxcImRpc2NvdW50X3R5cGVcIjpcImN1cnJlbmN5XCIsXCJudW1iZXJfYmlsbGluZ19jeWNsZXNcIjoxfSx7XCJ0aXRsZVwiOlwiWk9PTTMwXCIsXCJjb2RlXCI6XCJaT09NMzBcIixcInN0YXJ0X2RhdGVcIjpcIjIwMTktMDUtMjFUMDA6MDA6MDBaXCIsXCJlbmRfZGF0ZVwiOlwiMjAyMC0wMS0wMVQyMzo1OTo1OS45OTlaXCIsXCJkaXNjb3VudFwiOjMwMC4wLFwiZGlzY291bnRfdHlwZVwiOlwiY3VycmVuY3lcIixcIm51bWJlcl9iaWxsaW5nX2N5Y2xlc1wiOjF9LHtcInRpdGxlXCI6XCJaRUU1IFNCSSBZT05PXCIsXCJjb2RlXCI6XCJaRUU1WU9OT1wiLFwic3RhcnRfZGF0ZVwiOlwiMjAxOS0wMS0wNlQxODozODowMFpcIixcImVuZF9kYXRlXCI6XCIyMDE5LTA3LTAxVDE4OjI5OjU5Ljk5OVpcIixcImRpc2NvdW50XCI6MjQ5Ljc1LFwiZGlzY291bnRfdHlwZVwiOlwiY3VycmVuY3lcIixcIm51bWJlcl9iaWxsaW5nX2N5Y2xlc1wiOjF9LHtcInRpdGxlXCI6XCJaRUU1TkVUMzBcIixcImNvZGVcIjpcIlpFRTVORVQzMFwiLFwic3RhcnRfZGF0ZVwiOlwiMjAxOS0wNi0xNFQwMDowMDowMFpcIixcImVuZF9kYXRlXCI6XCIyMDIwLTAzLTMxVDIzOjU5OjU5Ljk5OVpcIixcImRpc2NvdW50XCI6MzAwLjAsXCJkaXNjb3VudF90eXBlXCI6XCJjdXJyZW5jeVwiLFwibnVtYmVyX2JpbGxpbmdfY3ljbGVzXCI6MX0se1widGl0bGVcIjpcIlpFRTVLT0RBS1wiLFwiY29kZVwiOlwiWkVFNUtPREFLXCIsXCJzdGFydF9kYXRlXCI6XCIyMDE5LTAyLTAyVDE4OjM4OjAwWlwiLFwiZW5kX2RhdGVcIjpcIjIwMjAtMDQtMDJUMTg6Mjk6NTkuOTk5WlwiLFwiZGlzY291bnRcIjoyMDAuMCxcImRpc2NvdW50X3R5cGVcIjpcImN1cnJlbmN5XCIsXCJudW1iZXJfYmlsbGluZ19jeWNsZXNcIjoxfSx7XCJ0aXRsZVwiOlwiU0lUSUJCMjVcIixcImNvZGVcIjpcIlNJVElCQjI1XCIsXCJzdGFydF9kYXRlXCI6XCIyMDE5LTA2LTEwVDAwOjAwOjAwWlwiLFwiZW5kX2RhdGVcIjpcIjIwMjAtMDQtMDFUMjM6NTk6NTkuOTk5WlwiLFwiZGlzY291bnRcIjoyNTAuMCxcImRpc2NvdW50X3R5cGVcIjpcImN1cnJlbmN5XCIsXCJudW1iZXJfYmlsbGluZ19jeWNsZXNcIjoxfSx7XCJ0aXRsZVwiOlwiR0VUMzBcIixcImNvZGVcIjpcIkdFVDMwXCIsXCJzdGFydF9kYXRlXCI6XCIyMDE5LTA1LTA5VDAwOjAwOjAwWlwiLFwiZW5kX2RhdGVcIjpcIjIwMTktMDYtMThUMjM6NTk6NTkuOTk5WlwiLFwiZGlzY291bnRcIjozMDAuMCxcImRpc2NvdW50X3R5cGVcIjpcImN1cnJlbmN5XCIsXCJudW1iZXJfYmlsbGluZ19jeWNsZXNcIjoxfSx7XCJ0aXRsZVwiOlwiTUlaRUU1XCIsXCJjb2RlXCI6XCJNSVpFRTVcIixcInN0YXJ0X2RhdGVcIjpcIjIwMTktMDUtMDFUMDA6MDA6MDBaXCIsXCJlbmRfZGF0ZVwiOlwiMjAxOS0wNS0zMVQyMzo1OTo1OS45OTlaXCIsXCJkaXNjb3VudFwiOjMwMC4wLFwiZGlzY291bnRfdHlwZVwiOlwiY3VycmVuY3lcIixcIm51bWJlcl9iaWxsaW5nX2N5Y2xlc1wiOjF9LHtcInRpdGxlXCI6XCJTVEFOQzIwXCIsXCJjb2RlXCI6XCJTVEFOQzIwXCIsXCJzdGFydF9kYXRlXCI6XCIyMDE5LTA1LTE2VDAwOjAwOjAwWlwiLFwiZW5kX2RhdGVcIjpcIjIwMjAtMDYtMDFUMjM6NTk6NTkuOTk5WlwiLFwiZGlzY291bnRcIjoyMDAuMCxcImRpc2NvdW50X3R5cGVcIjpcImN1cnJlbmN5XCIsXCJudW1iZXJfYmlsbGluZ19jeWNsZXNcIjoxfSx7XCJ0aXRsZVwiOlwiU0FNU1VORzMwXCIsXCJjb2RlXCI6XCJTQU1TVU5HMzBcIixcInN0YXJ0X2RhdGVcIjpcIjIwMTktMDUtMTNUMDA6MDA6MDBaXCIsXCJlbmRfZGF0ZVwiOlwiMjAyMC0wNC0wMVQyMzo1OTo1OS45OTlaXCIsXCJkaXNjb3VudFwiOjMwMC4wLFwiZGlzY291bnRfdHlwZVwiOlwiY3VycmVuY3lcIixcIm51bWJlcl9iaWxsaW5nX2N5Y2xlc1wiOjF9XSxcImFzc2V0X3R5cGVzXCI6WzYsMCw5XSxcImFzc2V0X2lkc1wiOltdLFwibnVtYmVyX29mX3N1cHBvcnRlZF9kZXZpY2VzXCI6NSxcIm1vdmllX2F1ZGlvX2xhbmd1YWdlc1wiOltdLFwidHZfc2hvd19hdWRpb19sYW5ndWFnZXNcIjpbXSxcImNoYW5uZWxfYXVkaW9fbGFuZ3VhZ2VzXCI6W10sXCJ2YWxpZF9mb3JfYWxsX2NvdW50cmllc1wiOnRydWV9LFwic3Vic2NyaXB0aW9uX3N0YXJ0XCI6XCIyMDE4LTExLTMwVDIwOjI4OjU4LjE2N1pcIixcInN1YnNjcmlwdGlvbl9lbmRcIjpcIjIwMTktMTEtMzBUMjM6NTk6NTlaXCIsXCJzdGF0ZVwiOlwiYWN0aXZhdGVkXCIsXCJyZWN1cnJpbmdfZW5hYmxlZFwiOmZhbHNlLFwicGF5bWVudF9wcm92aWRlclwiOlwiY3JtXCIsXCJmcmVlX3RyaWFsXCI6bnVsbCxcImNyZWF0ZV9kYXRlXCI6XCIyMDE4LTExLTMwVDIwOjI4OjU4LjE2N1pcIixcImFkZGl0aW9uYWxcIjp7XCJwYXltZW50aW5mb1wiOlwiWmVlSW50ZXJuYWxFbXBsb3llZXNcIn0sXCJhbGxvd2VkX2JpbGxpbmdfY3ljbGVzXCI6MCxcInVzZWRfYmlsbGluZ19jeWNsZXNcIjowfV0iLCJjdXJyZW50X2NvdW50cnkiOiJJTiIsInNjb3BlIjpbInN1YnNjcmlwdGlvbmFwaSIsInVzZXJhcGkiXSwiYW1yIjpbImRlbGVnYXRpb24iXX0.G6D5DR6CmbTm94LePnMPvijMaZqYsFB5dolVa8qAh8HWoH67m2koOJ86TVTGgWZgj8K9kA4pBYmKTmXZ-7lJIwlWZowBSpFbH_oAgKVa5QJM9t2D24Se-YY4MrdYB1czc6ivF6i-_JmTDzg7vcksvGkAVFkq26Kr6yLauu_9OXeVQqTJL-RBjmAW4tSEjY_hpstk7HvQqzqKZJ6GfPESsq_88HcM7yerhKjOPHNd1IidRmr18LPrPqFLcsXxBAC9HIYD6tGBjUj5fAxCNu8Y8Rm0irpKGO-R-UxcLrWj0TwM75iRjoJ4WDmCd3thEqIYCxpCgWz8EmbdPa93J7a1mA"



@interface ZEE5PlayerManager()<UIGestureRecognizerDelegate>
@property CustomControl *customControlView;
@property Zee5MenuView *customMenu;
@property Zee5MuteView *muteView;
@property ParentalControl *parentalControl;


@property ZEE5PlayerConfig *playerConfig;

@property(strong , nonatomic) UIView *viewPlayer;

@property(nonatomic) UIPanGestureRecognizer *panGesture;
@property(nonatomic) UITapGestureRecognizer *tapGesture;

@property(nonatomic) NSString *selectedString;

@property(nonatomic) NSMutableArray *subTitleArray;
@property(nonatomic) NSMutableArray *videoQualityArray;
@property(nonatomic) NSString *selectedSubtitle;
@property(nonatomic) NSString *selectedLangauge;
@property(nonatomic) NSInteger selectedQuality;

@property (weak, nonatomic) NSArray *audioTracks;
@property (weak, nonatomic) NSArray *textTracks;
@property (weak, nonatomic) NSArray *selectedTracks;


@property(nonatomic) BOOL isLive;

@property(nonatomic) BOOL videoCompleted;
@property(nonatomic) BOOL watchCredits;


@property(nonatomic) CGFloat previousDuration;

@property(nonatomic) NSInteger adIndex;

@property(nonatomic) NSTimeInterval startTime;
@property(nonatomic) NSTimeInterval endTime;
@property(nonatomic) NSString  *showID;
@property(nonatomic) Boolean seekStared;

@property(nonatomic) BOOL isContentSubscribed;
@property(nonatomic) NSString *countryId;
@property(nonatomic) NSString *translationId;

@property(nonatomic) NSTimer *creditsTimer;
@property(nonatomic) BOOL isTimerExits;
@property(nonatomic) NSInteger watchSeconds;



@end


@implementation ZEE5PlayerManager

static ZEE5PlayerManager *sharedManager = nil;
//static ChromeCastManager *castManager;

+ (ZEE5PlayerManager *)sharedInstance
{
    if (sharedManager) {
        return sharedManager;
    }
    
    static dispatch_once_t  t = 0;
    
    dispatch_once(&t, ^{
        sharedManager = [[ZEE5PlayerManager alloc] init];
//        castManager = [[ChromeCastManager alloc] init];
//        [castManager initializeCastOptions];

        //            imaSettings.enableOmidExperimentally = YES;
        AVAudioSession *session = [AVAudioSession sharedInstance];
        NSError *error;
        [session setCategory:AVAudioSessionCategoryPlayback error:&error];
        [session setActive:YES error:&error];
        NSBundle *bundel = [NSBundle bundleForClass:self.class];
//        [UIFont jbs_registerFontWithFilenameString:@"ZEE5_Player.ttf" bundle:bundel];
//
//        [[PlayKitManager sharedInstance] registerPlugin:IMAPlugin.self];
        
        [ZEE5UserDefaults setUserToken:TOKEN];
        
        
        [ZEE5PlayerManager sharedInstance].isContentSubscribed = NO;

        
    });
    
    return sharedManager;
}



- (void)playWithCurrentItem {
    
    [self getBase64StringwithCompletion:^(NSString *base64) {
        [[Zee5PlayerPlugin sharedInstance] initializePlayer:self.playbackView andItem:self.currentItem andLicenceURI:BaseUrls.drmLicenceUrl andBase64Cerificate:base64];
        
        self.panGesture = [[UIPanGestureRecognizer alloc]init];
        
        if (self.playerConfig.showCustomPlayerControls) {
            [self addCustomControls];
            self.customControlView.buttonPlay.selected = true;
        }
        else
        {
            [self addMuteViewToPlayer];
        }

        [self handleOrientations];
        [self handleTracks];
        [[ReportingManager sharedInstance] getWatchHistory];
        [[ReportingManager sharedInstance] startReportingWatchHistory];
        

    }];
   
}
- (void)playWithCurrentItemWithURL : (NSString *)urlString playbacksession_id:(NSString *)playbacksession_id{
    
    [[Zee5PlayerPlugin sharedInstance] initializePlayer:self.playbackView andURL:urlString andToken:@"" playbacksession_id:playbacksession_id];
    self.panGesture = [[UIPanGestureRecognizer alloc]init];
    
    if (self.playerConfig.showCustomPlayerControls) {
        [self addCustomControls];
        self.customControlView.buttonPlay.selected = true;
    }
    else
    {
        [self addMuteViewToPlayer];
        
    }
    
    [self handleOrientations];
    [self handleTracks];

}


-(void)registerNotifications
{
    [[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(orientationChanged:) name:UIDeviceOrientationDidChangeNotification object:[UIDevice currentDevice]];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didEnterForGround)
                                                 name:UIApplicationWillEnterForegroundNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onAudioInterruption) name:AVAudioSessionInterruptionNotification object:nil];
    
}

-(void)didEnterForGround
{
    if ( ([[UIDevice currentDevice] orientation] ==  UIDeviceOrientationLandscapeLeft) || ([[UIDevice currentDevice] orientation] ==  UIDeviceOrientationLandscapeRight) )
    {
        [self showFullScreen];
    }
    else
    {
        [self hideFullScreen];
    }
}
-(void)onAudioInterruption
{
    _customControlView.buttonPlay.hidden = NO;
    _customControlView.activityView.hidden = YES;
    [self hideUnHideTopView:NO];

    [NSObject cancelPreviousPerformRequestsWithTarget:self];
    [self pause];
}

- (void) orientationChanged:(NSNotification *)note
{
   
    
    UIDevice * device = note.object;
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        dispatch_sync(dispatch_get_main_queue(), ^{
            switch(device.orientation)
            {
                case UIDeviceOrientationLandscapeLeft :
                    [self showFullScreen];
                    break;
                case  UIDeviceOrientationLandscapeRight:
                    [self showFullScreen];
                    break;
                    
                case UIDeviceOrientationPortrait :
                    [self hideFullScreen];
                    break;
                case UIDeviceOrientationPortraitUpsideDown:
                    [self hideFullScreen];
                    break;
                default:
                    break;
            };
        });
        
    });
}


-(void)addCustomControls
{
    NSBundle *bundel = [NSBundle bundleForClass:self.class];
    if(_customControlView != nil )
    {
        [_customControlView.sliderLive animateToolTipFading:NO];
    }
    if(_customControlView == nil)
    {
        _customControlView = [[bundel loadNibNamed:@"CustomControl" owner:self options:nil] objectAtIndex:0];
        
    }
    _customControlView.frame = CGRectMake(0, 0, self.viewPlayer.frame.size.width, self.viewPlayer.frame.size.height);
    _customControlView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    _customControlView.buttonPlay.hidden = YES;
    _customControlView.activityView.hidden = NO;
//    _customControlView.con_width_more.constant = 0;
    _customControlView.viewLive.hidden = !self.isLive;
    _customControlView.viewVod.hidden = self.isLive;
    _customControlView.related = self.currentItem.related;
    _customControlView.btnSkipIntro.hidden = YES;
    _customControlView.con_height_watchCredits.constant = 0;
    

    if (self.isLive) {
        _customControlView.lableTitle.text = [NSString stringWithFormat:@"%@ : %@",self.currentItem.channel_Name,self.currentItem.showName];
        
        _customControlView.lableSubTitle.attributedText = [Utility addAttribuedFont:_startTime :_endTime];

        _customControlView.sliderLive.userInteractionEnabled = NO;
        _customControlView.sliderLive.defaultValue = _startTime;
        _customControlView.labelLiveCurrentTime.text = [Utility convertEpochToTime:_startTime];
        if (_endTime == 0) {
            [self refreshLabel];
        }
        else
        {
            _customControlView.labelLiveDuration.text = [Utility convertEpochToTime:_endTime];
        }
        
    }
    else
    {
        _customControlView.lableTitle.text = [NSString stringWithFormat:@"%@ : %@",self.currentItem.channel_Name,self.currentItem.channel_Name];

    }
    
    
    [self.playbackView addSubview:_customControlView];
    

    _tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapOnPlayer)];
    _tapGesture.delegate = self;
    [_tapGesture setDelaysTouchesBegan : YES];
    _tapGesture.numberOfTapsRequired = 1;

    [_customControlView addGestureRecognizer:_tapGesture];
    
    _panGesture = [[UIPanGestureRecognizer alloc]initWithTarget:self action:@selector(handlePanGesture:)];
    [_customControlView addGestureRecognizer:_panGesture];
    
    [Zee5PlayerPlugin sharedInstance].player.view.multipleTouchEnabled   = NO;
    [Zee5PlayerPlugin sharedInstance].player.view.userInteractionEnabled = YES;
    
    _customControlView.collectionView.hidden = YES;
    
    [_customControlView forwardAndRewindActions];
    [self showAllControls];
    

    
}

-(void)addMuteViewToPlayer
{
    NSBundle *bundel = [NSBundle bundleForClass:self.class];
    if(_muteView == nil)
    {
        _muteView = [[bundel loadNibNamed:@"Zee5MuteView" owner:self options:nil] objectAtIndex:0];
    }
    _muteView.frame = CGRectMake(0, 0, [Zee5PlayerPlugin sharedInstance].player.view.frame.size.width, [Zee5PlayerPlugin sharedInstance].player.view.frame.size.height);
    _muteView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    if ([self.currentItem.asset_subtype isEqualToString:@"episode"]) {
        _muteView.lblTitle.text = self.currentItem.channel_Name;
        NSString *date = [Utility convertFullDateToDate:self.currentItem.release_date];
        _muteView.lblEpisode.text = [NSString stringWithFormat:@"E%@ | %@",self.currentItem.episode_number,date];

    }
    else
    {
        _muteView.lblTitle.text = self.currentItem.channel_Name;
        _muteView.lblEpisode.text = @"";
    }
    [Zee5PlayerPlugin sharedInstance].player.volume = 0.0;
    [[Zee5PlayerPlugin sharedInstance].player.view addSubview:_muteView];
    
    
}


-(void)hideUnHideTopView:(BOOL )isHidden
{
    _customControlView.viewTop.hidden = isHidden;
    _customControlView.topView.hidden = isHidden;
    if (!self.isLive) {
        _customControlView.viewVod.hidden = isHidden;
        _customControlView.collectionView.hidden = self.customControlView.buttonFullScreen.selected ? isHidden : YES;
    }
    else
    {
        _customControlView.viewLive.hidden = isHidden;
        _customControlView.collectionView.hidden = self.customControlView.buttonLiveFull.selected ? isHidden : YES;

    }

}

-(void)hideCustomControls
{
    [self hideUnHideTopView:YES];
    self.panGesture.enabled = false;
}


- (void)updateRemainingTime
{
    
    self.customControlView.lblRemainingSecds.text = [NSString stringWithFormat:@"%ld seconds",(long)self.watchSeconds];
    self.watchSeconds = self.watchSeconds - 1;
    if(self.watchSeconds == 0)
    {
        [self pause];
        self.currentItem.endCreditsMarker  = @"";
        [self watchCreditsClicked];
        RelatedVideos *model = self.currentItem.related[0];
        //        [[ZEE5PlayerManager sharedInstance] playSimilarEvent:model.identifier];
        [[ZEE5PlayerManager sharedInstance] playVODContent:model.identifier country:@"IN"
                                               translation:@"en"];

    }
}

#pragma mark: Player Events

-(void)onPlaying
{
    self.customControlView.buttonPlay.selected = YES;
    _customControlView.sliderLive.userInteractionEnabled = YES;
    _videoCompleted = NO;

    [self showAllControls];
    _customControlView.buttonPlay.hidden = NO;
    _customControlView.activityView.hidden = YES;
    
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
    if (!_customControlView.topView.hidden) {
        [self perfomAction];
    }

}

-(void)onDurationUpdate:(PKEvent *)event
{
    if (self.currentItem.streamType == vod && !self.isLive) {
        [[ReportingManager sharedInstance] startReportingWatchHistory];
    }
    else{
        [[ReportingManager sharedInstance] stopWatchHistoryReporting];
    }

    for (ZEE5AdModel *model in self.currentItem.fanAds) {
        if ([model.time isEqualToString:POSTTIME]) {
            NSInteger duration = [event.duration integerValue];
            if(duration != 0)
            {
                model.time = [NSString stringWithFormat:@"%ld",(long)duration];
            }
        }
    }

}

-(void)onTimeChange:(PKEvent *)event
{
    CGFloat floored = floor([event.currentTime doubleValue]);
    NSInteger totalSeconds = (NSInteger) floored;
    [[Zee5FanAdManager sharedInstance] loadfanAds:totalSeconds];
    CGFloat remainingDuration = [self getTotalDuration] - [self getCurrentDuration];

    if (self.isLive) {
        if (!_customControlView.sliderLive.isTracking && !_seekStared) {
            _customControlView.sliderLive.value = totalSeconds;
            if(_customControlView.buttonLiveFull.selected)
            {
                [_customControlView.sliderLive updateToolTipView];
                [_customControlView.sliderLive animateToolTipFading:YES];
            }
            
        }
        _customControlView.sliderLive.maximumValue = [[Zee5PlayerPlugin sharedInstance] getDuration];
        
        if ((_customControlView.sliderLive.maximumValue - 10) <= _customControlView.sliderLive.value) {
            [_customControlView.buttonLive setTitle:@"LIVE" forState:UIControlStateNormal];
        }
        else
        {
            [_customControlView.buttonLive setTitle:@"GO LIVE" forState:UIControlStateNormal];
        }
    }
    else
    {
        if (self.currentItem.skipAvailable != nil)
        {
            NSString *startSeconds = [self.currentItem.skipAvailable valueForKey:@"intro_start_s"];
            NSString *endSeconds = [self.currentItem.skipAvailable valueForKey:@"intro_end_s"];
            
            NSInteger startTime = [Utility getSeconds:startSeconds];
            NSInteger endTime = [Utility getSeconds:endSeconds];
            if (totalSeconds >= startTime && totalSeconds < endTime) {
                self.customControlView.btnSkipIntro.hidden = NO;
            }
            else
            {
                self.customControlView.btnSkipIntro.hidden = YES;
            }

        }
        
        if (![self.currentItem.endCreditsMarker isEqualToString:@""])
        {
            NSInteger startTime = [Utility getSeconds:self.currentItem.endCreditsMarker];
            
            if(remainingDuration < startTime && remainingDuration > 15 && !self.watchCredits)
            {
                if (!self.isTimerExits) {
                    self.isTimerExits = YES;
                    self.creditsTimer =  [NSTimer scheduledTimerWithTimeInterval:1.0
                                                                          target:self
                                                                        selector:@selector(updateRemainingTime)
                                                                        userInfo:nil
                                                                         repeats:YES];
                    [self.creditsTimer fire];
                }
                

                [self hideUnHideTopView:NO];
                self.customControlView.con_height_watchCredits.constant = 50;
                [self handleUpwardsGesture:nil];

            }
            
            
            
        }

        if (remainingDuration < 5.0) {
            [self hideUnHideTopView:NO];
            [self handleUpwardsGesture:nil];
        }
      
        if (!_customControlView.sliderDuration.isTracking && !_seekStared) {
            _customControlView.sliderDuration.value = totalSeconds;
        }
        _customControlView.labelCurrentTime.text = [Utility getDuration:totalSeconds total:[[Zee5PlayerPlugin sharedInstance] getDuration]];
        
        _customControlView.labelTotalDuration.text = [Utility getDuration:[[Zee5PlayerPlugin sharedInstance] getDuration] total:[[Zee5PlayerPlugin sharedInstance] getDuration]];
        _customControlView.sliderDuration.maximumValue = [[Zee5PlayerPlugin sharedInstance] getDuration];
    }
}

-(void)onBuffring
{
    [self showAllControls];
    
    _customControlView.buttonPlay.hidden = YES;
    _customControlView.activityView.hidden = NO;
    [NSObject cancelPreviousPerformRequestsWithTarget:self];

}

-(void)onBuffringValueChange:(PKEvent *)event
{
    
    PKTimeRange *range = event.timeRanges.lastObject;
    if (_isLive) {
        _customControlView.sliderLive.maximumValue = [[Zee5PlayerPlugin sharedInstance] getDuration];
        _customControlView.bufferLiveProgress.progress = range.duration/100;
        
    }
    else
    {
        _customControlView.bufferProgress.progress = range.duration/100;
        if([_customControlView.labelTotalDuration.text isEqualToString:@"00:00"])
        {
            _customControlView.sliderDuration.maximumValue = [[Zee5PlayerPlugin sharedInstance] getDuration];
            _customControlView.labelTotalDuration.text = [Utility getDuration:[[Zee5PlayerPlugin sharedInstance] getDuration] total:[[Zee5PlayerPlugin sharedInstance] getDuration]];
            [_customControlView.sliderDuration setMarkerAtPosition:100.0/_customControlView.sliderDuration.maximumValue];
            [_customControlView.sliderDuration setMarkerAtPosition:200.0/_customControlView.sliderDuration.maximumValue];
        }
    }

}

- (void)onComplete
{
    if ([self.currentItem.related count] == 0) {
        _videoCompleted = YES;
        _customControlView.buttonPlay.hidden = YES;
        _customControlView.buttonReplay.hidden = NO;
        _customControlView.activityView.hidden = YES;
        [self hideUnHideTopView:NO];
        [NSObject cancelPreviousPerformRequestsWithTarget:self];
        
        if (self.delegate && [self.delegate respondsToSelector:@selector(didFinishPlaying)]) {
            [self.delegate didFinishPlaying];
        }
        
    }
    else
    {
        [self pause];
        RelatedVideos *model = self.currentItem.related[0];
//        [[ZEE5PlayerManager sharedInstance] playSimilarEvent:model.identifier];
        [[ZEE5PlayerManager sharedInstance] playVODContent:model.identifier country:@"IN"
                                               translation:@"en"];
        
    }
    
}


-(void)getTokenND:(void (^)(NSString *))completion
{
    [[NetworkManager sharedInstance] makeHttpRequest:@"GET" requestUrl:BaseUrls.getTokemNd requestParam:@{} requestHeaders:@{}  withCompletionHandler:^(id  _Nullable result) {
        
        NSLog(@"%@", result);
        if ([result isKindOfClass:[NSDictionary class]]) {
            
            completion(result[@"video_token"]);

            // [sharedManager playWithCurrentItem];
        }
        
    } failureBlock:^(ZEE5SdkError * _Nullable error) {
        //[self notifiyError:error];
    }];
    
}


-(void)getBase64StringwithCompletion:(void (^)(NSString *))completion
{
    NSURL *url = [[NSURL alloc] initWithString:BaseUrls.drmCertificateUrl];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
    request.HTTPMethod = @"GET";
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration] delegate:nil delegateQueue:[NSOperationQueue mainQueue]];
    
    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        NSString *base64 = [data base64EncodedStringWithOptions:0];
        completion(base64);
        [session finishTasksAndInvalidate];
        
    }];
    [postDataTask resume];

}

-(void)handleOrientations
{
    if (_playerConfig.shouldStartPlayerInLandScape) {
        [self showFullScreen];
        
    }
    else
    {
        [self didEnterForGround];
    }
}






-(void)perfomAction
{
    [self performSelector:@selector(hideCustomControls) withObject:nil afterDelay:HIDECONTROLSVALUE];
}

#pragma mark: Gesture Events

-(void)tapOnPlayer
{
    
    [_customControlView.sliderDuration animateToolTipFading:NO];
    if (_customControlView.topView.hidden) {
        [self hideUnHideTopView:NO];
    }
    else
    {
        [self hideUnHideTopView:YES];
    }

    
    
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
    if (_customControlView.buttonPlay.selected) {
        if (!_customControlView.topView.hidden) {
            [self perfomAction];
        }
        
    }
    self.panGesture.enabled = (self.customControlView.buttonLiveFull.selected && !_customControlView.topView.hidden && [AppConfigManager sharedInstance].config.isSimilarVideos);
    _customControlView.con_top_collectionView.constant = 10;
    
}
-(void)removeMenuView
{
    if(_customMenu != nil)
    {
        if (_customMenu.superview != nil) {
            [_customMenu removeFromSuperview];

        }
        return;
    }

}

- (void)handlePanGesture:(UIPanGestureRecognizer *)sender
{
    
    typedef NS_ENUM(NSUInteger, UIPanGestureRecognizerDirection) {
        UIPanGestureRecognizerDirectionUndefined,
        UIPanGestureRecognizerDirectionUp,
        UIPanGestureRecognizerDirectionDown,
        UIPanGestureRecognizerDirectionLeft,
        UIPanGestureRecognizerDirectionRight
    };
    
    static UIPanGestureRecognizerDirection direction = UIPanGestureRecognizerDirectionUndefined;
    
    switch (sender.state) {
            
        case UIGestureRecognizerStateBegan: {
            
            if (direction == UIPanGestureRecognizerDirectionUndefined) {
                
                CGPoint velocity = [sender velocityInView:[Zee5PlayerPlugin sharedInstance].player.view];
                
                BOOL isVerticalGesture = fabs(velocity.y) > fabs(velocity.x);
                
                if (isVerticalGesture) {
                    if (velocity.y > 0) {
                        direction = UIPanGestureRecognizerDirectionDown;
                    } else {
                        direction = UIPanGestureRecognizerDirectionUp;
                    }
                }
                
                else {
                    if (velocity.x > 0) {
                        direction = UIPanGestureRecognizerDirectionRight;
                    } else {
                        direction = UIPanGestureRecognizerDirectionLeft;
                    }
                }
            }
            
            break;
        }
            
        case UIGestureRecognizerStateChanged: {
            switch (direction) {
                case UIPanGestureRecognizerDirectionUp: {
                    [self handleUpwardsGesture:sender];
                    break;
                }
                case UIPanGestureRecognizerDirectionDown: {
                    [self handleDownwardsGesture:sender];
                    break;
                }
                default: {
                    break;
                }
            }
        }
            
        case UIGestureRecognizerStateEnded: {
            direction = UIPanGestureRecognizerDirectionUndefined;
            break;
        }
            
        default:
            break;
    }
    
}

- (void)handleUpwardsGesture:(UIPanGestureRecognizer *)sender
{
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
    _customControlView.con_top_collectionView.constant = -110;
    [UIView animateWithDuration:0.3 animations:^{
        [self.customControlView layoutIfNeeded];
    }];
}

- (void)handleDownwardsGesture:(UIPanGestureRecognizer *)sender
{
    if (_customControlView.buttonPlay.selected) {
        if (!_customControlView.topView.hidden) {
            [NSObject cancelPreviousPerformRequestsWithTarget:self];
            [self perfomAction];
        }
    }
    _customControlView.con_top_collectionView.constant = 10;
    [UIView animateWithDuration:0.3 animations:^{
        [self.customControlView layoutIfNeeded];
    }];
    
}

#pragma mark: Player Controls


-(void)play
{
    NSInteger rounded = roundf(_customControlView.sliderDuration.maximumValue);
    if (rounded == _customControlView.sliderDuration.value && rounded != 0) {
        [self onComplete];
    }
    else
    {
        self.customControlView.buttonPlay.selected = YES;
        [[Zee5PlayerPlugin sharedInstance].player play];
        [[ReportingManager sharedInstance] startReportingWatchHistory];
    }
}

-(void)pause
{
    self.customControlView.buttonPlay.selected = NO;
    [[Zee5PlayerPlugin sharedInstance].player pause];
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
    [[ReportingManager sharedInstance] stopWatchHistoryReporting];
}

-(void)replay
{
    [self showAllControls];
    _customControlView.buttonPlay.hidden = NO;
    _customControlView.activityView.hidden = YES;
    
    _customControlView.sliderDuration.value = 0.0;
    _customControlView.bufferProgress.progress = 0.0;
    _customControlView.labelCurrentTime.text = @"00:00";
    [self play];
    
}

- (void)stop
{
    [[Zee5PlayerPlugin sharedInstance].player pause];
    [[Zee5PlayerPlugin sharedInstance].player stop];
    [[ReportingManager sharedInstance] stopWatchHistoryReporting];
    self.isStop = YES;
    [[Zee5FanAdManager sharedInstance] stopFanAd];
}

-(void)setSeekTime:(NSInteger)value
{
    __weak __typeof(self) weakSelf = self;

    int rounded = roundf([[Zee5PlayerPlugin sharedInstance] getDuration]);
    if(value == 0)
    {
        value = 1;
    }
    else if(value == rounded)
    {
        value = value - 1;
    }
    _seekStared = YES;
    [[Zee5PlayerPlugin sharedInstance] setSeekTime:value];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        weakSelf.seekStared = false;
    });

    //    self.customControlView.buttonPlay.selected = YES;
    
}



-(void)setFullScreen:(BOOL)isFull
{
    if(isFull)
    {
        [[UIDevice currentDevice] setValue:@(UIInterfaceOrientationLandscapeRight) forKey:@"orientation"];
    }
    else
    {
        [[UIDevice currentDevice] setValue:@(UIInterfaceOrientationPortrait) forKey:@"orientation"];
    }
}
-(void)setMute:(BOOL)isMute
{
    [Zee5PlayerPlugin sharedInstance].player.volume = isMute ? 0.0 : 1.0;
    
}
-(void)setLock:(BOOL)isLock
{
    [self hideUnHideTopView:isLock];
    if (self.delegate && [self.delegate respondsToSelector:@selector(didTaponLockButton:)]) {
        [self.delegate didTaponLockButton:isLock ? Locked : Unlocked];
    }
    
}

-(void)tapOnNextButton
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(didTaponNextButton)]) {
        [self.delegate didTaponNextButton];
    }
}
-(void)tapOnPrevButton
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(didTaponPrevButton)]) {
        [self.delegate didTaponPrevButton];
    }
    
}
-(void)tapOnMinimizeButton
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(didTaponMinimizeButton)]) {
        [self.delegate didTaponMinimizeButton];
    }
    
}


-(void)tapOnLiveButton
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(didTaponLiveButton:)]) {
        [self.delegate didTaponLiveButton:self.showID];
    }
    
}

-(void)tapOnGoLiveButton
{
    [self setSeekTime:_customControlView.sliderLive.maximumValue];
    [self play];
}

-(void)navigateToDetail
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(didTapOnPlayerToNavigateDetail)]) {
        [self.delegate didTapOnPlayerToNavigateDetail];
    }

    
}

- (void)refreshLabel
{
    __weak __typeof(self) weakSelf = self;
    
    //refresh the label.text on the main thread
    dispatch_async(dispatch_get_main_queue(),^{
        
        
        NSString *time = [Utility convertEpochToTime:[NSDate timeIntervalSinceReferenceDate]];
        
        weakSelf.customControlView.labelLiveDuration.text = time;
        
    });
    // check every 60s
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [weakSelf refreshLabel];
    });
}


-(CGFloat )getCurrentDuration
{
    if ([Zee5PlayerPlugin sharedInstance].player != nil) {
        return [[Zee5PlayerPlugin sharedInstance] getCurrentTime];
    }
    return 0.0;
    
}
-(CGFloat )getTotalDuration
{
    if ([Zee5PlayerPlugin sharedInstance].player != nil) {
        return [[Zee5PlayerPlugin sharedInstance] getDuration];
    }
    return 0.0;
}


-(NSUInteger )getBufferPercentage
{
    if ([Zee5PlayerPlugin sharedInstance].player != nil) {
        return [[Zee5PlayerPlugin sharedInstance] getCurrentTime];
    }
    return 0;
}
-(void)showAllControls
{
    BOOL fullScreen = !self.customControlView.buttonLiveFull.selected;
    _customControlView.buttonReplay.hidden = YES;
    _customControlView.btnSkipNext.hidden = fullScreen;
    _customControlView.btnSkipPrev.hidden = fullScreen;
    
}




-(void)showFullScreen
{
    [_customControlView.sliderDuration animateToolTipFading:NO];
    _customControlView.sliderLive.fullScreen = YES;
    [_customControlView.sliderLive updateToolTipView];
    if(_customControlView.sliderLive.userInteractionEnabled)
    {
        [_customControlView.sliderLive animateToolTipFading:YES];
    }
    
    self.customControlView.buttonFullScreen.selected = YES;
    self.customControlView.buttonLiveFull.selected = YES;
    if(!_videoCompleted)
    {
        _customControlView.btnSkipNext.hidden = NO;
        _customControlView.btnSkipPrev.hidden = NO;
    }
    _customControlView.lableSubTitle.hidden = NO;
    
    self.customControlView.con_height_topBar.constant = TOPBARHEIGHT;
    self.customControlView.con_bottom_liveView.constant = -30;
    
    self.panGesture.enabled = (self.customControlView.buttonLiveFull.selected && !_customControlView.topView.hidden && [AppConfigManager sharedInstance].config.isSimilarVideos);
    self.customControlView.collectionView.hidden = ![AppConfigManager sharedInstance].config.isSimilarVideos;
    _customControlView.con_top_collectionView.constant = 10;
    

    
//    _customControlView.forwardButton.frame = CGRectMake(_customControlView.frame.size.width - 200, 0, 200, self.playbackView.frame.size.height);
//    _customControlView.rewindButton.frame = CGRectMake(0, 0, 200, self.playbackView.frame.size.height);
    


    
}

-(void)hideFullScreen
{
    _customControlView.sliderLive.fullScreen = NO;
    [_customControlView.sliderLive animateToolTipFading:NO];
    [_customControlView.sliderDuration animateToolTipFading:NO];
    
    self.customControlView.buttonFullScreen.selected = NO;
    self.customControlView.buttonLiveFull.selected = NO;
    _customControlView.btnSkipNext.hidden = YES;
    _customControlView.btnSkipPrev.hidden = YES;
    _customControlView.lableSubTitle.hidden = YES;
    
    self.customControlView.con_height_topBar.constant = 0;
    self.customControlView.con_bottom_liveView.constant = -10;
    
    
    self.panGesture.enabled = (self.customControlView.buttonLiveFull.selected && !_customControlView.topView.hidden && [AppConfigManager sharedInstance].config.isSimilarVideos);
    self.customControlView.collectionView.hidden = YES;
    _customControlView.con_top_collectionView.constant = 10;
    
    

//    _customControlView.forwardButton.frame = CGRectMake(_customControlView.frame.size.width - 150, 0, 150, self.playbackView.frame.size.height);
//    _customControlView.rewindButton.frame = CGRectMake(0, 0, 150, self.playbackView.frame.size.height);


}

-(void)forward:(NSInteger)value
{
    NSInteger currentTime = [[Zee5PlayerPlugin sharedInstance] getCurrentTime];
    NSInteger seekValue = currentTime + value;
    if(seekValue > [[Zee5PlayerPlugin sharedInstance] getDuration])
    {
        seekValue = [[Zee5PlayerPlugin sharedInstance] getDuration];
    }
    [self setSeekTime:seekValue];
}
-(void)rewind:(NSInteger)value
{
    NSInteger currentTime = [[Zee5PlayerPlugin sharedInstance] getCurrentTime];
    
    NSInteger seekValue = currentTime - value;
    if(seekValue < 0)
    {
        seekValue = 0;
    }
    [self setSeekTime:seekValue];
}

-(void)setAudioTrack:(NSString *)audioID
{
    self.selectedLangauge = audioID;
    
    [[Zee5PlayerPlugin sharedInstance].player selectTrackWithTrackId:audioID];
}

-(void)setSubTitle:(NSString *)subTitleID
{
    [[Zee5PlayerPlugin sharedInstance].player selectTrackWithTrackId:subTitleID];
}



// Handle Available Tracks and Present Them
- (void)handleTracks {
    // don't forget to use weak self to prevent retain cycles when needed
    __weak __typeof(self) weakSelf = self;
    
    // add observer to tracksAvailable event
    [[Zee5PlayerPlugin sharedInstance].player addObserver:self events:@[PlayerEvent.tracksAvailable, PlayerEvent.textTrackChanged, PlayerEvent.audioTrackChanged] block:^(PKEvent * _Nonnull event) {
        __strong __typeof(weakSelf) strongSelf = weakSelf;
        
        if ([event isKindOfClass:PlayerEvent.tracksAvailable]) {
            
            // Extract Audio Tracks
            if (event.tracks.audioTracks) {
                strongSelf.audioTracks = event.tracks.audioTracks;
                
                // Set Defualt array for Picker View
                strongSelf.selectedTracks = strongSelf.audioTracks;
                

            }
            // Extract Text Tracks
            if (event.tracks.textTracks) {
                strongSelf.textTracks = event.tracks.textTracks;
            }
        } else if ([event isKindOfClass:PlayerEvent.textTrackChanged]) {
            NSLog(@"selected text track:: %@", event.selectedTrack.title);
            self.selectedSubtitle = event.selectedTrack.title;
        } else if ([event isKindOfClass:PlayerEvent.audioTrackChanged]) {
            NSLog(@"selected audio track:: %@", event.selectedTrack.title);
        }
    }];
}


-(void)prepareCustomMenu
{
    NSBundle *bundel = [NSBundle bundleForClass:self.class];
    
    if(_customMenu == nil)
    {
        _customMenu = [[bundel loadNibNamed:@"Zee5MenuView" owner:self options:nil] objectAtIndex:0];
    }
    else
    {
        [_customMenu removeFromSuperview];
    }
    
    CGRect widowFrame = [[[UIApplication sharedApplication] keyWindow] frame];
    _customMenu.frame = CGRectMake(0, 0, widowFrame.size.width, widowFrame.size.height);
    _customMenu.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    

}
-(void)moreOptions
{
    [self prepareCustomMenu];

    NSMutableArray<Zee5MenuModel*> *models = [[NSMutableArray alloc] init];

    
    if ([self.audioTracks count] > 1) {
        Zee5MenuModel *model1 = [[Zee5MenuModel alloc] init];
        model1.imageName = @"T";
        model1.title = LANGUAGE;
        model1.type = 1;
        model1.isSelected = false;
        [models addObject:model1];
    }
    
    if ([_currentItem.subTitles count] > 0) {
        Zee5MenuModel *model2 = [[Zee5MenuModel alloc] init];
        model2.imageName = @"s";
        model2.title = SUBTITLES;
        model2.type = 1;
        model2.isSelected = false;
        [models addObject:model2];
    }


    Zee5MenuModel *model3 = [[Zee5MenuModel alloc] init];
    model3.imageName = @"2";
    model3.title = AUTOPLAY;
    model3.type = 1;
    model3.isSelected = false;
    [models addObject:model3];
    
    Zee5MenuModel *model4 = [[Zee5MenuModel alloc] init];
    model4.imageName = @"a";
    model4.title = WATCHLIST;
    model4.type = 1;
    model4.isSelected = false;
    [models addObject:model4];


    
    [_customMenu reloadDataWithObjects:models :false];
    [[[UIApplication sharedApplication] keyWindow] addSubview:_customMenu];
}

-(void)selectedMenuItem:(id)model
{
    Zee5MenuModel *menuModel = (Zee5MenuModel *)model;

    if ([menuModel.title  isEqualToString: LANGUAGE])
    {
        self.selectedString = menuModel.title;
        [self showLangaugeActionSheet];
    }
    else if ([menuModel.title  isEqualToString: SUBTITLES])
    {
        self.selectedString = menuModel.title;
        [self showSubtitleActionSheet];
    }
    
    else if([self.selectedString isEqualToString:LANGUAGE] )
    {
        [self removeMenuView];

        [self setAudioTrack:menuModel.idValue];
    }
    else if([self.selectedString isEqualToString:SUBTITLES] )
    {
        [self removeMenuView];
        self.selectedSubtitle = menuModel.title;
        

        [self setSubTitle:menuModel.idValue];
    }
    else if([self.selectedString isEqualToString:WATCHLIST] )
    {
        if (self.delegate && [self.delegate respondsToSelector:@selector(didTapOnAddToWatchList)]) {
            [self.delegate didTapOnAddToWatchList];
        }
    }
    else if([self.selectedString isEqualToString:AUTOPLAY] )
    {
        if (self.delegate && [self.delegate respondsToSelector:@selector(didTapOnEnableAutoPlay)]) {
            [self.delegate didTapOnEnableAutoPlay];
        }
    }
    
}



-(void)showSubtitleActionSheet
{
    self.selectedString = SUBTITLES;
    [self prepareCustomMenu];

    NSMutableArray<Zee5MenuModel*> *models = [[NSMutableArray alloc] init];
    
    
    Zee5MenuModel *model1 = [[Zee5MenuModel alloc] init];
    model1.imageName = @"s";
    model1.title = SUBTITLES;
    model1.type = 1;
    model1.isSelected = false;
    [models addObject:model1];
    
    
    Zee5MenuModel *model2 = [[Zee5MenuModel alloc] init];
    if ([self.selectedSubtitle.lowercaseString isEqualToString:@"Off".lowercaseString]) {
        model2.imageName = @"t";
        model2.isSelected = true;
    }
    else
    {
        model2.imageName = @"";
        model2.isSelected = false;
    }

    model2.title = @"Off";
    model2.type = 2;
    [models addObject:model2];

    for (NSString *str in _currentItem.subTitles) {
        Zee5MenuModel *model = [[Zee5MenuModel alloc] init];
        if ([self.selectedSubtitle isEqualToString:[Utility getLanguageStringFromId:str]]) {
            model.imageName = @"t";
            model.isSelected = true;
        }
        else
        {
            model.imageName = @"";
            model.isSelected = false;
        }

        model.title = [Utility getLanguageStringFromId:str];
        model.type = 2;
        model.idValue = [NSString stringWithFormat:@"%ld", [_currentItem.subTitles indexOfObject:str]];
        [models addObject:model];
    }
    
    [_customMenu reloadDataWithObjects:models : true];
    [[[UIApplication sharedApplication] keyWindow] addSubview:_customMenu];

}

-(void)airplayButtonClicked
{
    
//    [Zee5PlayerPlugin sharedInstance].player.
//    self.player.allowsExternalPlayback = true
//    self.player.usesExternalPlaybackWhileExternalScreenIsActive = true
//    let rect = CGRect(x: -100, y: 0, width: 0, height: 0)
//    let airplayVolume = MPVolumeView(frame: rect)
//    airplayVolume.showsVolumeSlider = false
//    self.view.addSubview(airplayVolume)
//    for view: UIView in airplayVolume.subviews {
//        if let button = view as? UIButton {
//            button.sendActions(for: .touchUpInside)
//            break
//        }
//    }
//    airplayVolume.removeFromSuperview()
}


-(void)skipIntroClicked
{
    NSString *endSeconds = [self.currentItem.skipAvailable valueForKey:@"intro_end_s"];
    NSInteger endTime = [Utility getSeconds:endSeconds];
    [self setSeekTime:endTime];

}

-(void)watchCreditsClicked
{
    self.watchCredits = YES;
    self.isTimerExits = NO;
    if (self.creditsTimer != nil) {
        [self.creditsTimer invalidate];
        self.creditsTimer = nil;
    }
    self.customControlView.con_height_watchCredits.constant = 0;
    [self handleDownwardsGesture:nil];

}
-(void)showLangaugeActionSheet
{
    self.selectedString = LANGUAGE;

    [self prepareCustomMenu];

    NSMutableArray<Zee5MenuModel*> *models = [[NSMutableArray alloc] init];
    
    
    Zee5MenuModel *model2 = [[Zee5MenuModel alloc] init];
    model2.imageName = @"T";
    model2.title = LANGUAGE;
    model2.type = 1;
    model2.isSelected = false;
    [models addObject:model2];

    for (Track *track in self.audioTracks) {
        Zee5MenuModel *model = [[Zee5MenuModel alloc] init];
        if ([self.selectedLangauge isEqualToString:track.id]) {
            model.imageName = @"t";
            model.isSelected = true;
        }
        else
        {
            model.imageName = @"";
            model.isSelected = false;
        }
        
        model.title = track.title;
        model.type = 2;
        model.idValue = track.id;
        [models addObject:model];
    }
    
    [_customMenu reloadDataWithObjects:models : true];
    [[[UIApplication sharedApplication] keyWindow] addSubview:_customMenu];
    
}


- (void)playLiveContent:(NSString *)channel_id showId:(NSString *)show_id startTime:(NSTimeInterval)startTime endtime:(NSTimeInterval)end_time episodNumber:(NSString *)episode_no seasonNumber:(NSString *)season_nun category:(NSString *)category translation:(NSString *)laguage playerConfig:(ZEE5PlayerConfig *)playerConfig playbackView:(PlayerView *)playbackView
{
    self.previousDuration = [[Zee5PlayerPlugin sharedInstance] getDuration];
    self.showID = show_id;
    if (self.previousDuration != 0) {
        [self watchDuration:self.currentItem.content_id];
    }
    [self registerNotifications];
    self.startTime = startTime;
    self.endTime = end_time;
    self.playerConfig = playerConfig;
    self.isLive = YES;
    self.isStop = NO;
    self.playbackView = playbackView;
    
    
    NSDictionary *headers = @{@"Content-Type":@"application/json",@"X-Access-Token":ZEE5UserDefaults.getPlateFormToken};
    NSString *end = @"";
    if (end_time == 0) {
        end = @"now";
    }
    else
    {
        end = [NSString stringWithFormat:@"%.f", end_time];
    }
    NSMutableDictionary *param =@{@"epgchannel_id":channel_id, @"epgshow_id":show_id, @"start_time":[NSString stringWithFormat:@"%.f", startTime], @"end_time":end, @"episode_num":episode_no, @"season_nun":season_nun, @"translation":laguage, @"category":category, @"country": @"IN"}.mutableCopy;
    
    [[NetworkManager sharedInstance] makeHttpGetRequest:BaseUrls.liveContentDetails requestParam:param requestHeaders:headers withCompletionHandler:^(id  _Nullable result) {
        if ([self.delegate respondsToSelector:@selector(playerData:)]) {
            [self.delegate playerData:result];
        }
        
        EpgContentDetailsDataModel *model = [EpgContentDetailsDataModel initFromJSONDictionary:result];
        
        if (model.videoType == vod && model.isDRM) {
            [self getDRMToken:model.identifier andDrmKey:model.drmKeyID withCompletionHandler:^(id  _Nullable result) {
                
                [self initilizePlayerWithEPGContent:model andDRMToken:[result valueForKey:@"drm"]];
                
            } failureBlock:^(ZEE5SdkError * _Nullable error) {
                [self notifiyError:error];
                [[ReportingManager sharedInstance] stopWatchHistoryReporting];
            }];
        }
        else{
            [self initilizePlayerWithEPGContent:model andDRMToken:@""];
        }
        
    } failureBlock:^(ZEE5SdkError * _Nullable error) {
        [self notifiyError:error];
    }];
    
}

-(void)downLoadAddConfig
{
    if (_playerConfig.shouldStartPlayerInLandScape) {
        [[UIDevice currentDevice] setValue:@(UIInterfaceOrientationLandscapeRight) forKey:@"orientation"];
    }
        NSDictionary *params = @{@"content_id":@"0-1-8934",@"platform_name":@"desktop_web"};
    
    
//    NSDictionary *params = @{@"content_id":self.currentItem.content_id,@"platform_name":@"apple_app"};
    [[NetworkManager sharedInstance] makeHttpGetRequest:BaseUrls.adConfig requestParam:params requestHeaders:@{} withCompletionHandler:^(id  _Nullable result) {
        NSLog(@"%@",result);
        NSMutableArray <ZEE5AdModel*>*fanAdModelArray = [[NSMutableArray alloc] init];
        NSMutableArray <ZEE5AdModel*>*googleAdModelArray = [[NSMutableArray alloc] init];
        
        NSArray *videoAds = [result ValueForKeyWithNullChecking:@"video_ads"];
        if([videoAds count] > 0)
        {
            
            for (NSDictionary *insideDict in videoAds) {
                for (NSDictionary *adDict in [insideDict ValueForKeyWithNullChecking:@"intervals"]) {
                    ZEE5AdModel *adModel = [ZEE5AdModel initFromJSONDictionary:adDict];
                    if ([adModel.tag containsString:@"http"]) {
                        [googleAdModelArray addObject:adModel];
                        
                    }
                    else
                    {
                        [fanAdModelArray addObject:adModel];
                    }
                }
            }
            
        }
        
        
        self.currentItem.fanAds =  fanAdModelArray;
        self.currentItem.googleAds =  googleAdModelArray;
        [self playWithCurrentItem];
//        [[Zee5FanAdManager sharedInstance] createFanAds];
        
    } failureBlock:^(ZEE5SdkError * _Nullable error) {
        NSLog(@"%@",error.message);
        [sharedManager playWithCurrentItem];
        
    }];
}

- (void)playVODContent:(NSString*)content_id country:(NSString*)country translation:(NSString*)laguage playerConfig:(ZEE5PlayerConfig*)playerConfig playbackView:(nonnull UIView *)playbackView
{
    
    _isStop = false;
    self.viewPlayer = playbackView;
    self.playbackView = [[PlayerView alloc] initWithFrame:CGRectMake(0, 0, playbackView.frame.size.width, playbackView.frame.size.height)];
    
    self.playbackView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;

    [self.viewPlayer addSubview:self.playbackView];
    
    [self registerNotifications];
    self.playerConfig = playerConfig;
    self.isLive = NO;
    
    [self playVODContent:content_id country:country translation:laguage];
}
- (void)playAESContent:(NSString*) content_id country:(NSString*)country translation:(NSString*) laguage platform_name:(NSString*)platform_name playbacksession_id:(NSString*)playbacksession_id playerConfig:(ZEE5PlayerConfig*)playerConfig playbackView:(nonnull UIView *)playbackView
{
    
    _isStop = false;
    self.viewPlayer = playbackView;
    self.playbackView = [[PlayerView alloc] initWithFrame:CGRectMake(0, 0, playbackView.frame.size.width, playbackView.frame.size.height)];
    
    self.playbackView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    
    [self.viewPlayer addSubview:self.playbackView];
    
    [self registerNotifications];
    self.playerConfig = playerConfig;
    
    self.isLive = NO;
    [self playAESContent:content_id country:country translation:laguage platform_name:platform_name playbacksession_id:playbacksession_id];
}

- (void)playVODWithURL:(NSString*)urlString playerConfig:(ZEE5PlayerConfig*)playerConfig playbackView:(nonnull PlayerView *)playbackView
{
    
    _isStop = false;
    self.playbackView = playbackView;
    [self registerNotifications];
    self.playerConfig = playerConfig;
    self.isLive = NO;
    [self playWithCurrentItemWithURL:urlString playbacksession_id:@""];
}

- (void)playVODWithJson:(NSDictionary *)dict playerConfig:(ZEE5PlayerConfig*)playerConfig playbackView:(nonnull PlayerView *)playbackView
{
    
    _isStop = false;
    self.playbackView = playbackView;
    [self registerNotifications];
    self.playerConfig = playerConfig;
    self.isLive = NO;
    
    VODContentDetailsDataModel *model = [VODContentDetailsDataModel initFromJSONDictionary:dict];
    
    if (model.isDRM) {
        [self getDRMToken:model.identifier andDrmKey:model.drmKeyID withCompletionHandler:^(id  _Nullable result) {
            
            [self initilizePlayerWithVODContent:model andDRMToken:[result valueForKey:@"drm"]];
            
        } failureBlock:^(ZEE5SdkError * _Nullable error) {
            [self notifiyError:error];
            [[ReportingManager sharedInstance] stopWatchHistoryReporting];
        }];
    }else{
        
        [self initilizePlayerWithVODContent:model andDRMToken:@""];
    }
}


- (void)playVODContent:(NSString*)content_id country:(NSString*)country translation:(NSString*)laguage
{
    self.countryId = country;
    self.translationId = laguage;
    self.watchCredits = NO;
    self.isTimerExits = NO;
    self.watchSeconds = 10;
    if (self.creditsTimer != nil) {
        [self.creditsTimer invalidate];
        self.creditsTimer = nil;
    }
    self.previousDuration = [[Zee5PlayerPlugin sharedInstance] getDuration];
    
    if (self.previousDuration != 0) {
        [self watchDuration:self.currentItem.content_id];
    }
    self.isLive = NO;
    
    NSString *urlString = [NSString stringWithFormat:@"%@/%@", BaseUrls.vodContentDetails, content_id];
    NSDictionary *param =@{@"country":country, @"translation":laguage};
    NSDictionary *headers = @{@"Content-Type":@"application/json",@"X-Access-Token":ZEE5UserDefaults.getPlateFormToken};
    [[NetworkManager sharedInstance] makeHttpGetRequest:urlString requestParam:param requestHeaders:headers withCompletionHandler:^(id result) {
        
        VODContentDetailsDataModel *model = [VODContentDetailsDataModel initFromJSONDictionary:result];
        
        
        [[NetworkManager sharedInstance] getSettingsWithCompletion:^(NSDictionary *dict) {
            
            if ([dict count] > 0) {
                if ([dict[@"age_rating"] isEqualToString:@"A"]) {
                    [self handleVodContent:model];
                }
                else if ([dict[@"age_rating"] isEqualToString:@"U/A"])
                {
                    if (![model.ageRating isEqualToString:@"A"] ) {
                        [self handleVodContent:model];
                    }
                    else
                    {
                        [self showParentControlPopUp:dict[@"pin"] andModel:model];
                    }
                }
                else
                {
                    if ([model.ageRating isEqualToString:@"U"])
                    {
                        [self handleVodContent:model];
                    }
                    else
                    {
                        [self showParentControlPopUp:dict[@"pin"] andModel:model];
                        
                    }
                }
            }
            else
            {
                [self handleVodContent:model];

            }
            
        } failureBlock:^(ZEE5SdkError * _Nullable error) {
            [self handleVodContent:model];
        }];
        
        
        
        
        
    } failureBlock:^(ZEE5SdkError *error) {
        NSLog(@"%@", error.message);
        [self notifiyError:error];
    }];
    
}

-(void)handleVodContent:(VODContentDetailsDataModel *)model
{
    if ([model.businessType containsString:@"premium"]) {
        [self getSubscrptionListWithModel:model];
    }
    else
    {
        if (model.isDRM) {
            [self getDRMToken:model.identifier andDrmKey:model.drmKeyID withCompletionHandler:^(id  _Nullable result) {
                
                [self initilizePlayerWithVODContent:model andDRMToken:[result valueForKey:@"drm"]];
                
            } failureBlock:^(ZEE5SdkError * _Nullable error) {
                [self notifiyError:error];
                [[ReportingManager sharedInstance] stopWatchHistoryReporting];
            }];
        }else{
            
            [self initilizePlayerWithVODContent:model andDRMToken:@""];
        }
    }
    
}

- (void)playAESContent:(NSString*)content_id country:(NSString*)country translation:(NSString*)laguage  platform_name:(NSString*)platform_name playbacksession_id:(NSString*)playbacksession_id
{
    self.previousDuration = [[Zee5PlayerPlugin sharedInstance] getDuration];
    
    if (self.previousDuration != 0) {
        [self watchDuration:self.currentItem.content_id];
    }
    self.isLive = NO;
    
    NSDictionary *param =@{@"content_id":content_id,@"country":country, @"translation":laguage,@"platform_name":platform_name,@"playbacksession_id":playbacksession_id};
    NSDictionary *headers = @{@"Content-Type":@"application/json",@"X-Access-Token":ZEE5UserDefaults.getPlateFormToken,@"x-playback-session-id" : playbacksession_id};
    [[NetworkManager sharedInstance] makeHttpGetRequest:BaseUrls.getndToken requestParam:param requestHeaders:headers withCompletionHandler:^(id result) {
        
        
        [self playWithCurrentItemWithURL:[result valueForKey:@"video_token"] playbacksession_id:playbacksession_id];
//        if
        
    } failureBlock:^(ZEE5SdkError *error) {
        NSLog(@"%@", error.message);
        [self notifiyError:error];
    }];
    
}


-(void)getTokenAndCustomDataFromContent:(NSString*)content_id country:(NSString*)country translation:(NSString*)laguage withCompletionHandler:(DRMSuccessHandler)success andFailure:(DRMFailureHandler)failed
{
    NSString *urlString = [NSString stringWithFormat:@"%@/%@", BaseUrls.vodContentDetails, content_id];
    NSDictionary *param =@{@"country":country, @"translation":laguage};
    NSDictionary *headers = @{@"Content-Type":@"application/json",@"X-Access-Token":ZEE5UserDefaults.getPlateFormToken};
    [[NetworkManager sharedInstance] makeHttpGetRequest:urlString requestParam:param requestHeaders:headers withCompletionHandler:^(id result) {
        
        VODContentDetailsDataModel *model = [VODContentDetailsDataModel initFromJSONDictionary:result];

        if (model.isDRM) {
            [self getDRMToken:model.identifier andDrmKey:model.drmKeyID withCompletionHandler:^(id  _Nullable result) {
                success(BaseUrls.drmLicenceUrl,[result valueForKey:@"drm"]);
                
            } failureBlock:^(ZEE5SdkError * _Nullable error) {
                failed(error.message);
            }];
        }else{
            failed(@"Error Message");

        }

    } failureBlock:^(ZEE5SdkError *error) {
        NSLog(@"%@", error.message);
        [self notifiyError:error];
    }];

}


- (void)initilizePlayerWithVODContent:(VODContentDetailsDataModel*)model andDRMToken:(NSString*)token
{
    self.currentItem = [[CurrentItem alloc] init];
    if (model.isDRM) {
        self.currentItem.hls_Url = [kCDN_URL stringByAppendingString:model.hlsUrl];
    }else{
        self.currentItem.hls_Url = model.hlsUrl;
    }
    self.currentItem.mpd_Url = [kCDN_URL stringByAppendingString:model.mpdUrl] ;

    self.currentItem.drm_token = token;
    self.currentItem.drm_key = model.drmKeyID;
    self.currentItem.subTitles = model.subtitleLanguages;
    self.currentItem.streamType = vod;
    self.currentItem.content_id = model.identifier;
    self.currentItem.channel_Name = model.title;
    self.currentItem.asset_type = model.assetType;
    self.currentItem.asset_subtype = model.assetSubtype;
    self.currentItem.release_date = model.releaseDate;
    self.currentItem.episode_number = model.episodeNumber;
    self.currentItem.skipAvailable = model.skipAvailable;
    self.currentItem.endCreditsMarker = model.endCreditsMarker;

    self.currentItem.geners = model.geners;
    self.currentItem.isDRM = model.isDRM;
    
    if (_playerConfig.playerType == normalPlayer) {
        [self getVodSimilarContent];
        [self downLoadAddConfig];
    }
    else
    {
         self.playerConfig.showCustomPlayerControls = false;
         [self playWithCurrentItem];
    }
}

- (void)initilizePlayerWithEPGContent:(EpgContentDetailsDataModel*)model andDRMToken:(NSString*)token
{
    self.currentItem = [[CurrentItem alloc] init];
    if (model.isDRM) {
        self.currentItem.hls_Url = [kCDN_URL stringByAppendingString:model.hlsUrl];
    }else{
        self.currentItem.hls_Url = model.hlsUrl;
    }
    self.currentItem.drm_token = token;
    self.currentItem.drm_key = model.drmKeyID;
    self.currentItem.subTitles = model.subtitleLanguages;
    self.currentItem.streamType = vod;
    self.currentItem.content_id = model.identifier;
    self.currentItem.channel_Name = model.channel_name;
    self.currentItem.asset_type = model.assetType;
    self.currentItem.asset_subtype = model.assetSubtype;
    self.currentItem.geners = model.geners;
    self.currentItem.isDRM = model.isDRM;
    self.currentItem.hls_Full_Url = model.hlsFullURL;
    self.currentItem.showName = model.show_name;
//    [sharedManager playWithCurrentItem];
    if (_playerConfig.playerType == normalPlayer) {
        [self downLoadAddConfig];
    }
    else
    {
        self.playerConfig.showCustomPlayerControls = false;
        [self playWithCurrentItem];
    }
    
}


- (void)getVodSimilarContent{
    
    NSDictionary *param =@{@"country":self.countryId,
                           @"translation":self.translationId,
                           @"languages":@"desc",
                           @"page":@"1",
                           @"limit":@"15",
                           @"user_type":@"free"
                           };
    NSDictionary *headers = @{@"Content-Type":@"application/json",@"X-Access-Token":ZEE5UserDefaults.getPlateFormToken};
    NSString *urlString = [NSString stringWithFormat:@"%@/%@",BaseUrls.vodSimilarContent,self.currentItem.content_id];
    
    
    [[NetworkManager sharedInstance] makeHttpGetRequest:urlString requestParam:param requestHeaders:headers withCompletionHandler:^(id  _Nullable result) {
        
        SimilarDataModel *model = [SimilarDataModel initFromJSONDictionary:result];
        self.currentItem.related = model.relatedVideos;
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"kRefreshRelatedVideoList" object:nil];
        
        
    } failureBlock:^(ZEE5SdkError * _Nullable error) {
        
    }];
    
}

- (void)getSubscrptionListWithModel:(VODContentDetailsDataModel *)contentModel
{
    [[NetworkManager sharedInstance] getCountry:^(NSString *strCountry) {
        
        [self handleSubscrption:strCountry andModel:contentModel];
        
    } failureBlock:^(ZEE5SdkError * _Nullable error) {
        
    }];
    
    
}


-(void)showParentControlPopUp:(NSString *)pin andModel:(VODContentDetailsDataModel *)model
{
    __weak __typeof(self) weakSelf = self;

    NSBundle *bundel = [NSBundle bundleForClass:self.class];
    if(_parentalControl == nil)
    {
        _parentalControl = [[bundel loadNibNamed:@"ParentalControl" owner:self options:nil] objectAtIndex:0];
    }
    CGRect widowFrame = [[[UIApplication sharedApplication] keyWindow] frame];
    _parentalControl.strPin = pin;
    _parentalControl.isSuccess = ^(Boolean success) {
        if(success)
        {
            [weakSelf handleVodContent:model];
        }
    };
    
    
    _parentalControl.frame = CGRectMake(0, 0, widowFrame.size.width, widowFrame.size.height);
    _parentalControl.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    [[[UIApplication sharedApplication] keyWindow] addSubview:_parentalControl];
}

-(void)handleSubscrption:(NSString *)strCountry andModel:(VODContentDetailsDataModel *)contentModel
{
    NSDictionary *param =@{@"translation":self.translationId,
                           @"include_all":@"false",
                           
                           };
    NSString *userToken = [NSString stringWithFormat:@"bearer %@", ZEE5UserDefaults.getUserToken];
    
    NSDictionary *headers = @{@"Content-Type":@"application/json",@"Authorization":userToken};
    
    [[NetworkManager sharedInstance] makeHttpGetRequest:BaseUrls.getSubscription requestParam:param requestHeaders:headers withCompletionHandler:^(id  _Nullable result) {
        
        for(NSDictionary *dict in result)
        {
            SubscriptionModel *model = [[SubscriptionModel alloc] initWithDictionary:dict];
            if ([model.subscriptionPlan.countries containsObject:strCountry]) {
                
                NSArray *aryLangauge = [[NSArray alloc] init];
                if ([contentModel.assetType isEqualToString:@"0"]) {
                    aryLangauge = model.subscriptionPlan.movieAudioLanguages;
                }
                else if ([contentModel.assetType isEqualToString:@"1"] || [contentModel.assetType isEqualToString:@"6"]) {
                    aryLangauge = model.subscriptionPlan.tvShowAudioLanguages;
                }
                else
                {
                    aryLangauge = model.subscriptionPlan.channelAudioLanguages;
                }
                
                if([aryLangauge count] == 0)
                {
                    self.isContentSubscribed = YES;
                    break;
                }
                else
                {
                    for (NSString *langauge in contentModel.languages) {
                        if([aryLangauge containsObject:langauge])
                        {
                            self.isContentSubscribed = YES;
                            break;
                        }
                    }
                }
                
            }
        }
        if (self.isContentSubscribed) {
            if (contentModel.isDRM) {
                [self getDRMToken:contentModel.identifier andDrmKey:contentModel.drmKeyID withCompletionHandler:^(id  _Nullable result) {
                    
                    [self initilizePlayerWithVODContent:contentModel andDRMToken:[result valueForKey:@"drm"]];
                    
                } failureBlock:^(ZEE5SdkError * _Nullable error) {
                    [self notifiyError:error];
                    [[ReportingManager sharedInstance] stopWatchHistoryReporting];
                }];
            }else{
                
                [self initilizePlayerWithVODContent:contentModel andDRMToken:@""];
            }
        }
        else
        {
            [self playTrailerOrPromo:contentModel];
            //Play trailer or preview
            
        }
        
        
    } failureBlock:^(ZEE5SdkError * _Nullable error) {
        
    }];

}

-(void)playTrailerOrPromo : (VODContentDetailsDataModel *)contetModel
{
    NSPredicate *trailerPredicate = [NSPredicate predicateWithFormat:@"assetSubtype contains[c] %@",@"trailer"];
    NSArray *aryTrailer = [contetModel.relatedVideos filteredArrayUsingPredicate:trailerPredicate];
    if (aryTrailer.count > 0) {
        RelatedVideos *related = aryTrailer[0];
        [self playVODContent:related.identifier country:self.countryId translation:self.translationId];
        return;
    }
    
    NSPredicate *promoPredicate = [NSPredicate predicateWithFormat:@"assetSubtype contains[c] %@",@"promo"];
    NSArray *aryPromo = [contetModel.relatedVideos filteredArrayUsingPredicate:promoPredicate];
    if (aryPromo.count > 0) {
        RelatedVideos *related = aryPromo[0];
        [self playVODContent:related.identifier country:self.countryId translation:self.translationId];
        return;
    }
    
    NSPredicate *previewPredicate = [NSPredicate predicateWithFormat:@"assetSubtype contains[c] %@",@"preview"];
    NSArray *aryPreview = [contetModel.relatedVideos filteredArrayUsingPredicate:previewPredicate];
    if (aryPreview.count > 0) {
        RelatedVideos *related = aryPreview[0];
        [self playVODContent:related.identifier country:self.countryId translation:self.translationId];
        return;
    }
    
    //Subscrption pop up
    
}

- (void)getDRMToken:(NSString*)content_id andDrmKey:(NSString*)drmKey  withCompletionHandler:(SuccessHandler)success failureBlock:(FailureHandler)failure
{
    
    NSString *idfaString = [[[ASIdentifierManager sharedManager] advertisingIdentifier] UUIDString];
    NSLog(@"%@", idfaString);
    
    NSDictionary *parameterList = @{@"country":@"IN",
                                    @"persistent":@"false",
                                    @"key_id":drmKey,
                                    @"asset_id":content_id,
                                    @"token":TOKEN,
                                    @"entitlement_provider":@"internal",
                                    @"device_id":@"WebBrowser",
                                    @"request_type":@"Drm",
                                    };
    NSDictionary *headers = @{@"Content-Type":@"application/json"};
    
    
    [[NetworkManager sharedInstance] makeHttpRequest:@"POST" requestUrl:BaseUrls.entitlementV4 requestParam:parameterList requestHeaders:headers  withCompletionHandler:^(id  _Nullable result) {
        
        NSLog(@"%@", result);
        if ([result isKindOfClass:[NSDictionary class]]) {
            
            success(result);
            
            // [sharedManager playWithCurrentItem];
        }
        
    } failureBlock:^(ZEE5SdkError * _Nullable error) {
        //[self notifiyError:error];
        failure(error);
    }];
}

- (void)notifiyError:(ZEE5SdkError*)error{
    
    if ([self.delegate respondsToSelector:@selector(didFinishWithError:)]) {
        [self.delegate didFinishWithError:error];
    }
}


-(void)handleHLSError
{
    [self getTokenND:^(NSString *token) {
        NSArray *aryStrings = [self.currentItem.hls_Full_Url componentsSeparatedByString:@".m3u8"];
        if ([aryStrings count] > 1) {
            NSString *newURL = [NSString stringWithFormat:@"%@.m3u8%@",aryStrings[0],token];
            self.currentItem.hls_Full_Url = newURL;
            [self playWithCurrentItem];
        }
        
        NSLog(@"tokjen %@,%@",token,self.currentItem.hls_Full_Url);
    }];
}



- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    if ([touch.view isDescendantOfView:_customControlView.collectionView] || [touch.view isDescendantOfView:_customControlView.sliderDuration] || [touch.view isDescendantOfView:_customControlView.sliderLive] || [touch.view isDescendantOfView:_customMenu.tblView] ) {
        return NO;
    }
    return YES;
}




#pragma mark: GA Events

-(void)watchDuration:(NSString *)content_id
{
    
    NSDictionary *requestParams = @{
                                    @"cd48": content_id ? content_id : @"",
                                    @"cm5": [NSString stringWithFormat:@"%f",self.previousDuration],
                                    };
    
    [[ReportingManager sharedInstance] gaEventManager:requestParams];
    
}



- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIDeviceOrientationDidChangeNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationWillEnterForegroundNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:AVAudioSessionInterruptionNotification object:nil];
}

@end





