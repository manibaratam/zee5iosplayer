//
//  MyAdapter.m
//  ZEE5PlayerSDK
//
//  Created by Mani on 24/04/19.
//  Copyright © 2019 ZEE5. All rights reserved.
//

#import "MyAdapter.h"

@implementation MyAdapter

-(void)updateRequestAdapterWith:(id<Player>)player {}

- (PKRequestParams * _Nonnull)adaptWithRequestParams:(PKRequestParams * _Nonnull)requestParams {
    return [[PKRequestParams alloc]
            initWithUrl:requestParams.url headers:@{@"customData": self.customData}];
}

@end
