//
//  MyAdapter.h
//  ZEE5PlayerSDK
//
//  Created by Mani on 24/04/19.
//  Copyright © 2019 ZEE5. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <PlayKit/PlayKit-Swift.h>

NS_ASSUME_NONNULL_BEGIN

@interface MyAdapter : NSObject <PKRequestParamsAdapter>
@property NSString* customData;

@end

NS_ASSUME_NONNULL_END
