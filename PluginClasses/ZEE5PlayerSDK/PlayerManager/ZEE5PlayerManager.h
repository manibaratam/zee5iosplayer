//
//  ZEE5PlayerManager.h
//  ZEE5PlayerSDK
//
//  Created by admin on 04/12/18.
//  Copyright © 2018 ZEE5. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "ZEE5PlayerDelegate.h"
#import "CurrentItem.h"
#import <PlayKit/PlayKit-Swift.h>
#import "ZEE5PlayerConfig.h"
typedef void(^DRMSuccessHandler)(NSString* _Nullable licenceURL, NSString* _Nullable customData);
typedef void(^DRMFailureHandler)(NSString* _Nullable error);


NS_ASSUME_NONNULL_BEGIN

@interface ZEE5PlayerManager : NSObject
@property CurrentItem *currentItem;
@property (weak, nonatomic) id <ZEE5PlayerDelegate> delegate;
@property(strong , nonatomic) PlayerView *playbackView;
@property(nonatomic) BOOL isStop;


+ (ZEE5PlayerManager *)sharedInstance;

/*!
 * @discussion This method used to play live catch up and live dvr
 * @param channel_id Need to pass channel id
 * @param show_id Need to pass show id.
 * @param startTime Need to pass start time of catch up.
 * @param end_time Need to pass end time of catch up.
 * @param episode_no Need to pass episode number.
 * @param season_nun Need to pass season number.
 * @param category Need to pass category.
 * @param laguage Need to pass transaltion langauge id.
 * @param playerConfig Need to pass playerConfig file.
 * @param playbackView Need to pass the view where you want to play the video.
 */

- (void)playLiveContent:(NSString *)channel_id showId:(NSString *)show_id startTime:(NSTimeInterval)startTime endtime:(NSTimeInterval)end_time episodNumber:(NSString *)episode_no seasonNumber:(NSString *)season_nun category:(NSString *)category translation:(NSString *)laguage playerConfig:(ZEE5PlayerConfig *)playerConfig playbackView:(PlayerView *)playbackView;

/*!
 * @discussion This method used to play vod content with content id
 * @param content_id Need to pass content id of the video
 * @param country Need to pass user country id.
 * @param laguage Need to pass transaltion langauge id.
 * @param playerConfig Need to pass playerConfig file.
 * @param playbackView Need to pass the view where you want to play the video.
 */

- (void)playVODContent:(NSString*)content_id country:(NSString*)country translation:(NSString*)laguage playerConfig:(ZEE5PlayerConfig*)playerConfig playbackView:(UIView*)playbackView;

/*!
 * @discussion This method used to play vod content with json
 * @param playerConfig Need to pass playerConfig file.
 * @param playbackView Need to pass the view where you want to play the video.
 */

- (void)playVODWithJson:(NSDictionary *)dict playerConfig:(ZEE5PlayerConfig*)playerConfig playbackView:(nonnull PlayerView *)playbackView;

/*!
 * @discussion This method used to play vod content with url
 * @param playerConfig Need to pass playerConfig file.
 * @param playbackView Need to pass the view where you want to play the video.
 */

- (void)playVODWithURL:(NSString*)urlString playerConfig:(ZEE5PlayerConfig*)playerConfig playbackView:(nonnull PlayerView *)playbackView;

- (void)playVODContent:(NSString*)content_id country:(NSString*)country translation:(NSString*)laguage;

/*!
 * @discussion This method used to play hls content with content id
 * @param content_id Need to pass content id of the video
 * @param country Need to pass user country id.
 * @param laguage Need to pass transaltion langauge id.
 * @param platform_name Need to pass name of platform.
 * @param playbacksession_id Need to pass plassback session id for analytics.
 * @param playerConfig Need to pass playerConfig file.
 * @param playbackView Need to pass the view where you want to play the video.
 */
- (void)playAESContent:(NSString*) content_id country:(NSString*)country translation:(NSString*) laguage platform_name:(NSString*)platform_name playbacksession_id:(NSString*)playbacksession_id playerConfig:(ZEE5PlayerConfig*)playerConfig playbackView:(nonnull UIView *)playbackView;

/*!
 * @discussion This method use to get the custom data and licence url with content id
 * @param content_id Need to pass content id of the video
 * @param country Need to pass user country id.
 * @param laguage Need to pass transaltion langauge id.
 */

-(void)getTokenAndCustomDataFromContent:(NSString*)content_id country:(NSString*)country translation:(NSString*)laguage withCompletionHandler:(DRMSuccessHandler)success andFailure:(DRMFailureHandler)failed;


/*!
 * @discussion This method used to stop the video
 */
-(void)stop;

/*!
 * @discussion This method used to play the video
 */
-(void)play;

/*!
 * @discussion This method used to pause the video
 */
-(void)pause;
-(void)replay;
-(void)setMute:(BOOL)isMute;
-(void)setLock:(BOOL)isLock;
-(void)setFullScreen:(BOOL)isFull;
-(void)forward:(NSInteger)value;
-(void)rewind:(NSInteger)value;
-(void)setSeekTime:(NSInteger)value;
-(void)hideCustomControls;
-(void)perfomAction;
-(void)setAudioTrack:(NSString *)audioID;
-(void)setSubTitle:(NSString *)subTitleID;
-(void)moreOptions;
-(void)tapOnLiveButton;
-(void)tapOnGoLiveButton;
-(void)tapOnPrevButton;
-(void)tapOnNextButton;
-(void)navigateToDetail;
-(void)tapOnMinimizeButton;
-(void)airplayButtonClicked;
-(void)skipIntroClicked;
-(CGFloat )getCurrentDuration;
-(CGFloat )getTotalDuration;
-(NSUInteger )getBufferPercentage;
-(void)playSimilarEvent:(NSString *)content_id;
-(void)onPlaying;
-(void)onDurationUpdate:(PKEvent *)event;
-(void)onTimeChange:(PKEvent *)event;
-(void)onBuffring;
-(void)onBuffringValueChange:(PKEvent *)event;
-(void)handleHLSError;

-(void)hideUnHideTopView:(BOOL )isHidden;

-(void)selectedMenuItem:(id)model;
-(void)tapOnPlayer;
-(void)onComplete;

-(void)showLangaugeActionSheet;
-(void)showSubtitleActionSheet;

- (void)playWithCurrentItem ;
-(void)watchCreditsClicked;

@end

NS_ASSUME_NONNULL_END
