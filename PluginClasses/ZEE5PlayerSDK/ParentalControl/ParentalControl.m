//
//  ParentalControl.m
//  ZEE5PlayerSDK
//
//  Created by Mani on 27/08/19.
//  Copyright © 2019 ZEE5. All rights reserved.
//

#import "ParentalControl.h"

@interface ParentalControl()<UITextFieldDelegate>

@end


@implementation ParentalControl

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidHide:) name:UIKeyboardWillHideNotification object:nil];
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapOutSide)];
    [self addGestureRecognizer:tapGesture];
    
    self.txtPin.delegate = self;

}

- (void)keyboardDidShow:(NSNotification *)notification
{
    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;

    self.con_bottom_view.constant = keyboardSize.height - 25;
    
    [UIView animateWithDuration:0.3 animations:^{
        [self layoutIfNeeded];
        [self layoutSubviews];
    }];
    
}

-(void)keyboardDidHide:(NSNotification *)notification
{
    self.con_bottom_view.constant = -25;
    [UIView animateWithDuration:0.3 animations:^{
        [self layoutIfNeeded];
        [self layoutSubviews];
    }];
}


-(void)tapOutSide
{
    [self endEditing:YES];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self endEditing:YES];
    return YES;
}

- (IBAction)btnNextClicked:(UIButton *)sender {
    [self endEditing:YES];
    
    if ([self.txtPin.text isEqualToString:@""]) {
        [self showAlert:@"Please enter pin"];
    }
    else if(![self.txtPin.text isEqualToString:self.strPin])
    {
        [self showAlert:@"Please enter valid pin"];
    }
    else
    {
        [self removeFromSuperview];
        self.isSuccess(YES);
    }

}

-(void)showAlert:(NSString *)message
{
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert"
                                                    message:message
                                                   delegate:self
                                          cancelButtonTitle:nil
                                          otherButtonTitles:@"OK",nil];
    [alert show];

    
//    UIAlertController *controler = [UIAlertController alertControllerWithTitle:@"Alert" message:message preferredStyle:UIAlertControllerStyleAlert];
//    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
//    [controler addAction:okAction];
//
//    [self.controler presentViewController:controler animated:YES completion:nil];

}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
