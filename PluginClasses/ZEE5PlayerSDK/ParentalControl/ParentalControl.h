//
//  ParentalControl.h
//  ZEE5PlayerSDK
//
//  Created by Mani on 27/08/19.
//  Copyright © 2019 ZEE5. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ParentalControl : UIView

@property (nonatomic) NSString *strPin;

@property (weak, nonatomic) IBOutlet UITextField *txtPin;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *con_bottom_view;

@property (nonatomic, strong)  void (^isSuccess)(Boolean);

@end

NS_ASSUME_NONNULL_END
