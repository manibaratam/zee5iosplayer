//
//  Zee5CollectionCell.h
//  ZEE5PlayerSDK
//
//  Created by admin on 13/12/18.
//  Copyright © 2018 ZEE5. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RelatedVideos.h"
NS_ASSUME_NONNULL_BEGIN

@interface Zee5CollectionCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UILabel *labelTitle;

- (void)configureCellWith:(RelatedVideos *)model;

@end

NS_ASSUME_NONNULL_END
