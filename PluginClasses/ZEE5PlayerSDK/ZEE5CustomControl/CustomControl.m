//
//  CustomControl.m
//  ZEE5PlayerSDK
//
//  Created by admin on 11/12/18.
//  Copyright © 2018 ZEE5. All rights reserved.
//

#import "CustomControl.h"
#import "ZEE5PlayerManager.h"
#import "ZEE5PlayerConfig.h"
#import "Zee5CollectionCell.h"
@implementation CustomControl

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
- (void)awakeFromNib
{
    [super awakeFromNib];
    NSBundle *bundel = [NSBundle bundleForClass:self.class];
    UIImage *image = [UIImage imageNamed:@"Thumb" inBundle:bundel compatibleWithTraitCollection:nil];

    _sliderDuration.showTooltip = YES;
    [_sliderDuration setThumbImage:image forState:UIControlStateNormal];
    [_sliderDuration setThumbImage:image forState:UIControlStateHighlighted];
    
    _sliderLive.showTooltip = YES;
    _sliderLive.showTooltipContiously = YES;
    [_sliderLive setThumbImage:image forState:UIControlStateNormal];
    [_sliderLive setThumbImage:image forState:UIControlStateHighlighted];


    [_sliderDuration addTarget:self action:@selector(sliderValueChanged:withEvent:) forControlEvents:UIControlEventTouchUpOutside];
    [_sliderDuration addTarget:self action:@selector(sliderValueChanged:withEvent:) forControlEvents:UIControlEventTouchUpInside];
    
    
    [_sliderLive addTarget:self action:@selector(sliderValueChanged:withEvent:) forControlEvents:UIControlEventTouchUpOutside];
    [_sliderLive addTarget:self action:@selector(sliderValueChanged:withEvent:) forControlEvents:UIControlEventTouchUpInside];

    
    [_collectionView registerNib:[UINib nibWithNibName:@"Zee5CollectionCell" bundle:bundel] forCellWithReuseIdentifier:@"cell"];
    
    _buttonLive.layer.cornerRadius = _buttonLive.frame.size.height/2;
    _buttonLive.clipsToBounds = YES;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshRelatedList) name:@"kRefreshRelatedVideoList" object:nil];
    
    
}

-(void)forwardAndRewindActions
{
//    NSInteger value = 10;
////    NSInteger value = [self.labelForward.text integerValue];
//    __weak typeof(self) weakSelf = self;
//
//    [self.forwardButton removeFromSuperview];
//    
//    self.forwardButton = [[TouchableButton alloc] initWithTitle:@"Forward" imageName:@"3" seekBtn:@"Forward"];
//    [self insertSubview:self.forwardButton atIndex:1];
////    [self sendSubviewToBack:self.forwardButton];
//    
//    
//    self.forwardButton.singleTouch = ^(BOOL  touch) {
//        [weakSelf.forwardButton resetViews];
//        [[ZEE5PlayerManager sharedInstance] tapOnPlayer];
//
//    };
//    self.forwardButton.pressed = ^(BOOL pressed) {
//        
//        [[ZEE5PlayerManager sharedInstance] hideCustomControls];
//        [[ZEE5PlayerManager sharedInstance] forward:value];
//    };
//    
//    [self.rewindButton removeFromSuperview];
//    
//    self.rewindButton = [[TouchableButton alloc] initWithTitle:@"Rewind" imageName:@"N" seekBtn:@"Rewind"];
//    
//    [self insertSubview:self.rewindButton atIndex:1];
////    [self sendSubviewToBack:self.rewindButton];
//    
//    
//    self.rewindButton.singleTouch = ^(BOOL  touch) {
//        [weakSelf.rewindButton resetViews];
//        [[ZEE5PlayerManager sharedInstance] tapOnPlayer];
//    };
//    self.rewindButton.pressed = ^(BOOL pressed) {
//        
//        [[ZEE5PlayerManager sharedInstance] hideCustomControls];
//        [[ZEE5PlayerManager sharedInstance] rewind:value];
//    };
}



- (void)refreshRelatedList
{
    self.related = [ZEE5PlayerManager sharedInstance].currentItem.related;
    [self.collectionView reloadData];
}

- (IBAction)buttonPlayClicked:(UIButton *)sender {
    sender.selected = !sender.selected;
    if(sender.selected)
    {
        [[ZEE5PlayerManager sharedInstance] play];
    }
    else
    {
        [[ZEE5PlayerManager sharedInstance] pause];
    }
}
- (IBAction)btnMoreClicked:(UIButton *)sender {
    [[ZEE5PlayerManager sharedInstance] moreOptions];
}

- (IBAction)buttonFullScreenClicked:(UIButton *)sender {
    sender.selected = !sender.selected;
    [[ZEE5PlayerManager sharedInstance] setFullScreen:sender.selected];
}

- (IBAction)buttonLockClicked:(UIButton *)sender {
    sender.selected = !sender.selected;
    [[ZEE5PlayerManager sharedInstance] setLock:sender.selected];

}
- (IBAction)buttonMuteClicked:(UIButton *)sender {
    sender.selected = !sender.selected;
    [[ZEE5PlayerManager sharedInstance] setMute:sender.selected];

}
- (IBAction)buttonSkipNextClicked:(UIButton *)sender {
    [[ZEE5PlayerManager sharedInstance] tapOnNextButton];

}
- (IBAction)buttonSkipPrevClicked:(UIButton *)sender {
    [[ZEE5PlayerManager sharedInstance] tapOnPrevButton];

}
- (IBAction)buttonMinimizeClicked:(UIButton *)sender {
    [[ZEE5PlayerManager sharedInstance] tapOnMinimizeButton];

}


- (IBAction)buttonReplayClicked:(UIButton *)sender {
    [[ZEE5PlayerManager sharedInstance] replay];

}

- (IBAction)buttonLiveClicked:(UIButton *)sender {
    if ([sender.titleLabel.text isEqualToString:@"LIVE"]) {
        [[ZEE5PlayerManager sharedInstance] tapOnLiveButton];

    }
    else
    {
        [[ZEE5PlayerManager sharedInstance] tapOnGoLiveButton];
    }
    
}

- (IBAction)btnShareClicked:(UIButton *)sender {
}


- (IBAction)btnWatchListClicked:(UIButton *)sender {
}

- (IBAction)btnCastClicked:(UIButton *)sender {
}
- (IBAction)btnWatchPromoClicked:(id)sender {
}
- (IBAction)btnAudioLangaugeClicked:(UIButton *)sender {
    [[ZEE5PlayerManager sharedInstance] showLangaugeActionSheet];
}
- (IBAction)btnSubtitlsClicked:(UIButton *)sender {
    [[ZEE5PlayerManager sharedInstance] showSubtitleActionSheet];

}
- (IBAction)btnAirPlayClicked:(UIButton *)sender {
    [[ZEE5PlayerManager sharedInstance] airplayButtonClicked];

}


- (IBAction)btnSkipIntroClicked:(UIButton *)sender {
    [[ZEE5PlayerManager sharedInstance] skipIntroClicked];
}
- (IBAction)btnWatchCreditsClicked:(UIButton *)sender {
    [[ZEE5PlayerManager sharedInstance] watchCreditsClicked];

}


- (void)sliderValueChanged:(UISlider *)sender  withEvent:(UIEvent*)event
{
    [[ZEE5PlayerManager sharedInstance] setSeekTime:sender.value];

}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section;
{
    return [self.related count];
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
//    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];

    Zee5CollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    RelatedVideos *model = self.related[indexPath.row];
    [cell configureCellWith:model];
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    self.con_top_collectionView.constant = 10;
    [UIView animateWithDuration:0.3 animations:^{
        [self layoutIfNeeded];
    } completion:^(BOOL finished) {
        [[ZEE5PlayerManager sharedInstance] pause];
        RelatedVideos *model = self.related[indexPath.row];
        
//        [[ZEE5PlayerManager sharedInstance] playSimilarEvent:model.identifier];

        [[ZEE5PlayerManager sharedInstance] playVODContent:model.identifier country:@"IN"
                                              translation:@"en"];

    }];
}


- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"kRefreshRelatedVideoList" object:nil];
}

@end
