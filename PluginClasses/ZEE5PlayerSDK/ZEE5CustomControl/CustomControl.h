//
//  CustomControl.h
//  ZEE5PlayerSDK
//
//  Created by admin on 11/12/18.
//  Copyright © 2018 ZEE5. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Zee5Slider.h"
#import "RelatedVideos.h"
#import "Zee5MenuView.h"

//#import "ZEE5PlayerSDK/ZEE5PlayerSDK-Swift.h"


NS_ASSUME_NONNULL_BEGIN

@interface CustomControl : UIView



@property (weak, nonatomic) IBOutlet NSLayoutConstraint *con_height_topBar;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *con_top_collectionView;
@property (weak, nonatomic) IBOutlet UIButton *buttonReplay;

@property (weak, nonatomic) IBOutlet UIButton *buttonPlay;
@property (weak, nonatomic) IBOutlet UIButton *buttonFullScreen;
@property (weak, nonatomic) IBOutlet UILabel *labelCurrentTime;
@property (weak, nonatomic) IBOutlet UILabel *labelTotalDuration;
@property (weak, nonatomic) IBOutlet Zee5Slider *sliderDuration;

@property (weak, nonatomic) IBOutlet UIView *topView;

@property (weak, nonatomic) IBOutlet UIProgressView *bufferProgress;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityView;
@property (weak, nonatomic) IBOutlet UIView *viewVod;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *con_width_more;

@property (nonatomic) NSArray <RelatedVideos*>*related;


//Live
@property (weak, nonatomic) IBOutlet UILabel *labelLiveCurrentTime;
@property (weak, nonatomic) IBOutlet UILabel *labelLiveDuration;
@property (weak, nonatomic) IBOutlet UIProgressView *bufferLiveProgress;
@property (weak, nonatomic) IBOutlet UIButton *buttonLive;
@property (weak, nonatomic) IBOutlet UIView *viewLive;
@property (weak, nonatomic) IBOutlet Zee5Slider *sliderLive;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *con_bottom_liveView;
@property (weak, nonatomic) IBOutlet UIButton *buttonLiveFull;
@property (weak, nonatomic) IBOutlet UIButton *btnSkipNext;
@property (weak, nonatomic) IBOutlet UIButton *btnSkipPrev;
@property (weak, nonatomic) IBOutlet UILabel *lableTitle;
@property (weak, nonatomic) IBOutlet UILabel *lableSubTitle;
@property (weak, nonatomic) IBOutlet UIButton *btnMinimize;
@property (weak, nonatomic) IBOutlet UIView *viewTop;
@property (weak, nonatomic) IBOutlet UIButton *btnSkipIntro;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *con_height_watchCredits;
@property (weak, nonatomic) IBOutlet UIButton *btnWatchCredits;
@property (weak, nonatomic) IBOutlet UILabel *lblRemainingSecds;



//@property (strong, nonatomic)  TouchableButton *forwardButton;
//@property (strong, nonatomic)  TouchableButton *rewindButton;

-(void)forwardAndRewindActions;

@end

NS_ASSUME_NONNULL_END
