#import <UIKit/UIKit.h>

@interface SubscriptionAdditional : NSObject

@property (nonatomic, strong) NSString * paymentinfo;

-(instancetype)initWithDictionary:(NSDictionary *)dictionary;

-(NSDictionary *)toDictionary;
@end