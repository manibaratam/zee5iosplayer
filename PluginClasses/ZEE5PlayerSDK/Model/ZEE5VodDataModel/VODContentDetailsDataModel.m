//
//  VODContentDetailsDataModel.m
//  ZEE5PlayerSDK
//
//  Created by admin on 23/12/18.
//  Copyright © 2018 ZEE5. All rights reserved.
//

#import "VODContentDetailsDataModel.h"
#import "ZEE5UserDefaults.h"
#import "BaseUrls.h"
#import "Utility.h"
#import "ZEE5SdkError.h"
#import "NetworkManager.h"
#import "AppConfigManager.h"
#import "ReportingManager.h"
#import "NSDictionary+Extra.h"
#import "Genres.h"
#import "RelatedVideos.h"
#import "ZEE5PlayerManager.h"

@implementation VODContentDetailsDataModel

+ (instancetype)initFromJSONDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithNSDictionary:dict];
}

- (instancetype)initWithNSDictionary:(NSDictionary *)dict{
    
    self = [super init];
    
    if (self && [dict isKindOfClass:NSDictionary.class]) {
        self.identifier = [dict ValueForKeyWithNullChecking:@"id"];
        self.title = [dict ValueForKeyWithNullChecking:@"title"];
        self.businessType = [dict ValueForKeyWithNullChecking:@"business_type"];
        self.drmKeyID = [dict ValueForKeyWithNullChecking:@"drm_key_id"];
        self.subtitleLanguages = [dict ValueForKeyWithNullChecking:@"subtitle_languages"];
        self.languages = [dict ValueForKeyWithNullChecking:@"languages"];
        
        if ([[dict allKeys] containsObject:@"skip_available"]) {
            self.skipAvailable = [dict ValueForKeyWithNullChecking:@"skip_available"];
        }
        self.endCreditsMarker = [dict ValueForKeyWithNullChecking:@"end_credits_marker"];
        
        self.assetType = [NSString stringWithFormat:@"%d", [[dict ValueForKeyWithNullChecking:@"asset_type"]intValue]];
        self.assetSubtype = [dict ValueForKeyWithNullChecking:@"asset_subtype"];
        self.releaseDate = [dict ValueForKeyWithNullChecking:@"release_date"];
        self.episodeNumber = [[dict ValueForKeyWithNullChecking:@"episode_number"] integerValue];
        self.ageRating = [dict ValueForKeyWithNullChecking:@"age_rating"];
        
        
        if ([[dict ValueForKeyWithNullChecking:@"hls"] isKindOfClass:NSArray.class]) {
            NSArray *urls = [dict ValueForKeyWithNullChecking:@"hls"];
            self.hlsUrl = [urls firstObject];
        }
        if ([[dict ValueForKeyWithNullChecking:@"video"] isKindOfClass:NSArray.class]) {
            NSArray *urls = [dict ValueForKeyWithNullChecking:@"video"];
            self.mpdUrl = [urls firstObject];
        }
        
        if ([[dict ValueForKeyWithNullChecking:@"is_drm"] isKindOfClass:NSNumber.class]) {
            self.isDRM = [[dict ValueForKeyWithNullChecking:@"is_drm"] boolValue];
        }
        
        NSMutableArray <Genres*>*genreArray = [[NSMutableArray alloc] init];
        
        if ([[dict ValueForKeyWithNullChecking:@"genre"] isKindOfClass:NSArray.class]) {
            
            for (NSDictionary *genreDict in [dict ValueForKeyWithNullChecking:@"genre"]) {
                
                Genres *genre = [[Genres alloc] init];
                genre.identifier = [genreDict ValueForKeyWithNullChecking:@"id"];
                genre.value = [genreDict ValueForKeyWithNullChecking:@"value"];
                
                [genreArray addObject:genre];
            }
            self.geners = genreArray;
        }
        
        NSMutableArray <RelatedVideos*>*relatedVideosArray = [[NSMutableArray alloc] init];
        
        if ([[dict ValueForKeyWithNullChecking:@"related"] isKindOfClass:NSArray.class]) {
            
            for (NSDictionary *relatedDict in [dict ValueForKeyWithNullChecking:@"related"]) {
                RelatedVideos *related = [[RelatedVideos alloc] init];
                related.identifier = [relatedDict valueForKey:@"id"];
                related.imageURL = [relatedDict valueForKey:@"image_url"];
                related.title = [relatedDict valueForKey:@"title"];
                related.assetSubtype = [relatedDict valueForKey:@"asset_subtype"];
                
                [relatedVideosArray addObject:related];
            }
            self.relatedVideos = relatedVideosArray;
        }
    }
    
    return self;
}

@end
