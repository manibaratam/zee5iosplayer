//
//  VODContentDetailsDataModel.h
//  ZEE5PlayerSDK
//
//  Created by admin on 23/12/18.
//  Copyright © 2018 ZEE5. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Genres.h"
#import "RelatedVideos.h"

NS_ASSUME_NONNULL_BEGIN

@interface VODContentDetailsDataModel : NSObject
@property (nonatomic, copy)   NSString *identifier;
@property (nonatomic, copy)   NSString *title;
@property (nonatomic, copy)   NSString *drmKeyID;
@property (nonatomic, copy)   NSArray *subtitleLanguages;
@property (nonatomic, copy)   NSArray *languages;

@property (nonatomic, copy)   NSString *assetType;
@property (nonatomic, copy)   NSString *assetSubtype;
@property (nonatomic)   NSInteger episodeNumber;
@property (nonatomic, copy)   NSString *releaseDate;

@property (nonatomic, copy)   NSString *descriptionContent;
@property (nonatomic, copy)   NSString *contentLength;
@property (nonatomic, copy)   NSString *businessType;
@property (nonatomic, copy)   NSDictionary *skipAvailable;
@property (nonatomic, copy)   NSString *endCreditsMarker;
@property (nonatomic, copy)   NSString *ageRating;


@property (nonatomic, copy)   NSString *hlsUrl;
@property (nonatomic, copy)   NSString *mpdUrl;

@property (nonatomic, readwrite)  BOOL isDRM;
@property (nonatomic, copy) NSArray<Genres *> *geners;
@property (nonatomic, copy) NSArray<RelatedVideos *> *relatedVideos;


+ (instancetype)initFromJSONDictionary:(NSDictionary *)dict;

@end

NS_ASSUME_NONNULL_END
