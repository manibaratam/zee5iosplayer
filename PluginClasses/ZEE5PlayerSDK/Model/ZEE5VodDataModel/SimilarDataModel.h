//
//  SimilarDataModel.h
//  ZEE5PlayerSDK
//
//  Created by admin on 24/12/18.
//  Copyright © 2018 ZEE5. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ZEE5UserDefaults.h"
#import "BaseUrls.h"
#import "Utility.h"
#import "ZEE5SdkError.h"
#import "NetworkManager.h"
#import "AppConfigManager.h"
#import "ReportingManager.h"
#import "NSDictionary+Extra.h"
#import "Genres.h"
#import "RelatedVideos.h"
#import "ZEE5PlayerManager.h"

NS_ASSUME_NONNULL_BEGIN

@interface SimilarDataModel : NSObject
+ (instancetype)initFromJSONDictionary:(NSDictionary *)dict;

@property (nonatomic, copy) NSArray<RelatedVideos *> *relatedVideos;

@end

NS_ASSUME_NONNULL_END
