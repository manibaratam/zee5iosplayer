//
//  ZEE5ConfigDataModel.m
//  ZEE5PlayerSDK
//
//  Created by admin on 23/12/18.
//  Copyright © 2018 ZEE5. All rights reserved.
//

#import "ZEE5ConfigDataModel.h"
#import "NSDictionary+Extra.h"
@implementation ZEE5ConfigDataModel

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.contentAllowed = @[];
        self.adsURL = @"";
        self.authKey = @"";
        self.registerPartnerURL = @"";
        self.vodContentDetailURL = @"";
        self.liveContentURL = @"";
    }
    return self;
}

+ (instancetype)initFromJSONDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithNSDictionary:dict];
}

- (instancetype)initWithNSDictionary:(NSDictionary *)dict{
    
    self = [super init];

    if (self && [dict isKindOfClass:NSDictionary.class]) {
        self.contentAllowed = [dict ValueForKeyWithNullChecking:@"content_allowed"];
        self.adsURL = [dict ValueForKeyWithNullChecking:@"ads_url"];
        self.authKey = [dict ValueForKeyWithNullChecking:@"auth_key"];
        self.isSimilarVideos = [[dict ValueForKeyWithNullChecking:@"similar_videos"] boolValue];
        self.registerPartnerURL = [dict ValueForKeyWithNullChecking:@"register_partner_url"];
        self.vodContentDetailURL = [dict ValueForKeyWithNullChecking:@"vod_content_detail_url"];
        self.liveContentURL = [dict ValueForKeyWithNullChecking:@"live_content_url"];
        self.isConvivaEnabled = [[dict ValueForKeyWithNullChecking:@"conviva_enabled"] boolValue];
        self.isGaEnabled = [[dict ValueForKeyWithNullChecking:@"ga_enabled"] boolValue];
        self.isMetadataPush = [[dict ValueForKeyWithNullChecking:@"metadata_push"] boolValue];
    }
    
    return self;
}
@end
