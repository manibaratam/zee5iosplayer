//
//  ReportingManager.h
//  ZEE5PlayerSDK
//
//  Created by admin on 21/12/18.
//  Copyright © 2018 ZEE5. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ZEE5PlayerManager.h"
#import "ZEE5UserDefaults.h"
#import "NetworkManager.h"
NS_ASSUME_NONNULL_BEGIN

@interface ReportingManager : NSObject
+ (ReportingManager *)sharedInstance;
- (void)getWatchHistory;
- (void)startReportingWatchHistory;
- (void)reportWatchHistory;
- (void)stopWatchHistoryReporting;
- (void)gaEventManager:(NSDictionary *)dict;
@end

NS_ASSUME_NONNULL_END
