//
//  ReportingManager.m
//  ZEE5PlayerSDK
//
//  Created by admin on 21/12/18.
//  Copyright © 2018 ZEE5. All rights reserved.
//

#import "ReportingManager.h"


@interface ReportingManager()
@property(strong, nonatomic) NSTimer* timer;
@property (readwrite, nonatomic) BOOL isAlreadyExist;
@end

@implementation ReportingManager
static ReportingManager *sharedManager = nil;
+ (ReportingManager *)sharedInstance
{
    if (sharedManager) {
        return sharedManager;
    }
    
    static dispatch_once_t  t = 0;
    
    dispatch_once(&t, ^{
        sharedManager = [[ReportingManager alloc] init];
    });
    
    return sharedManager;
}

- (void)startReportingWatchHistory
{
    _isAlreadyExist = false;
    self.timer =  [NSTimer scheduledTimerWithTimeInterval:30.0
                                     target:self
                                   selector:@selector(reportWatchHistory)
                                   userInfo:nil
                                    repeats:YES];
    [self.timer fire];
}

- (void)reportWatchHistory
{
    if([ZEE5PlayerManager sharedInstance].currentItem == nil)
    {
        return;
    }
    NSLog(@"reporting: %.f", floor([[ZEE5PlayerManager sharedInstance] getCurrentDuration]));
    
    NSString *requestName = @"POST";
    
    if (_isAlreadyExist) {
        requestName = @"PUT";
    }
    
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    // or @"yyyy-MM-dd hh:mm:ss a" if you prefer the time with AM/PM
    
    //CurrentItem *item = [ZEE5PlayerManager sharedInstance].currentItem;
    
    NSString *currentDate = [dateFormatter stringFromDate:[NSDate date]];
    NSDictionary *requestParams = @{
        @"id": [ZEE5PlayerManager sharedInstance].currentItem.content_id,
        @"asset_type": [ZEE5PlayerManager sharedInstance].currentItem.asset_type,
        @"duration": [NSString stringWithFormat:@"%.f",[[ZEE5PlayerManager sharedInstance] getCurrentDuration]],
        @"date": currentDate
    };
    NSLog(@"%@", requestParams);
    NSString *userToken = [NSString stringWithFormat:@"bearer %@", ZEE5UserDefaults.getUserToken];
    NSDictionary *requestheaders = @{@"Content-Type":@"application/json", @"authorization": userToken};

    [[NetworkManager sharedInstance] makeHttpRequest:requestName requestUrl:BaseUrls.watchHistory requestParam:requestParams requestHeaders:requestheaders withCompletionHandler:^(id  _Nullable result) {
        NSLog(@"%@", result);
    } failureBlock:^(ZEE5SdkError * _Nullable error) {
        NSLog(@"%@", error.message);
        if (error.code == 400) {
            self.isAlreadyExist = true;
            [self reportWatchHistory];
        }
        else {
            self.isAlreadyExist = false;
            [self stopWatchHistoryReporting];
        }
    }];
   
}

- (void)getWatchHistory
{
    if([ZEE5PlayerManager sharedInstance].currentItem == nil)
    {
        return;
    }
    
    NSString *requestName = @"GET";
    
    NSDictionary *requestParams = @{
                                    @"country": @"IN",
                                    @"translation": [ZEE5PlayerManager sharedInstance].currentItem.asset_type                                    };
    NSLog(@"%@", requestParams);
    NSString *userToken = [NSString stringWithFormat:@"bearer %@", ZEE5UserDefaults.getUserToken];

    NSDictionary *requestheaders = @{@"Content-Type":@"application/json", @"authorization": userToken,@"X-Access-Token":ZEE5UserDefaults.getPlateFormToken};
    
    [[NetworkManager sharedInstance] makeHttpRequest:requestName requestUrl:BaseUrls.watchHistory requestParam:requestParams requestHeaders:requestheaders withCompletionHandler:^(id  _Nullable result) {
        NSMutableArray *array = [[NSMutableArray alloc] initWithArray:result];
        
        if ([array count] > 0) {
//            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"self.id == %d",];
//            NSArray *filterArray = [array filteredArrayUsingPredicate:predicate];
            
        }
        
    } failureBlock:^(ZEE5SdkError * _Nullable error) {
        
    }];
    
}
- (void)gaEventManager:(NSDictionary *)dict
{
    if ([AppConfigManager sharedInstance].config.authKey == nil) {
        return;
    }
    NSDictionary *requestParams = @{
                                    @"v": @"1",
                                    @"tid": @"UA-106326967-24",
                                    @"cid": [Utility getAdvertisingIdentifier],
                                    @"aid":  [AppConfigManager sharedInstance].config.authKey,
                                    @"t": @"event"
                                    };
    NSMutableDictionary *dictParams = [[NSMutableDictionary alloc]init];
    [dictParams addEntriesFromDictionary:dict];
    [dictParams addEntriesFromDictionary:requestParams];

    //NSLog(@"%@", dictParams);
    
    [[NetworkManager sharedInstance] makeHttpGetRequest:BaseUrls.googleAnalytic requestParam:dictParams requestHeaders:@{} withCompletionHandler:^(id  _Nullable result) {
        //NSLog(@"%@", result);

    } failureBlock:^(ZEE5SdkError * _Nullable error) {
        //NSLog(@"%@", error.message);

    }];
    
    
}

- (void)stopWatchHistoryReporting
{
    [self.timer invalidate];
}
@end
