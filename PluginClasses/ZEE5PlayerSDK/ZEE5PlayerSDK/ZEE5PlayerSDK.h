//
//  ZEE5PlayerSDK.h
//  ZEE5PlayerSDK
//
//  Created by admin on 04/12/18.
//  Copyright © 2018 ZEE5. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for ZEE5PlayerSDK.
FOUNDATION_EXPORT double ZEE5PlayerSDKVersionNumber;

//! Project version string for ZEE5PlayerSDK.
FOUNDATION_EXPORT const unsigned char ZEE5PlayerSDKVersionString[];

// In this header, you should import all the public headers of your framework using statements like

typedef enum devEnvironment
{
    development,
    production
} DevelopmentEnvironment;




#import "ZEE5SdkError.h"
#import "ZEE5PlayerManager.h"
#import "ZEE5PlayerConfig.h"
#import "ZEE5PlayerDelegate.h"
#import "ZEE5AudioTrack.h"
#import "ZEE5Subtitle.h"
#import "ZEE5LangaugeModel.h"
#import "CurrentItem.h"
#import "Genres.h"
#import "RelatedVideos.h"

#import "ZEE5UserDefaults.h"
#import "BaseUrls.h"
#import "Utility.h"
#import "ZEE5SdkError.h"
#import "NetworkManager.h"
#import "AppConfigManager.h"
#import "ReportingManager.h"
#import "NSDictionary+Extra.h"
#import "Genres.h"
#import "RelatedVideos.h"
#import "ZEE5PlayerManager.h"


#define kCDN_URL @"https://zee5vod.akamaized.net"

typedef void(^SuccessHandler)(id result);
typedef void(^FailureHandler)(ZEE5SdkError *error);

@interface ZEE5PlayerSDK : NSObject
+ (void)initializeWithApplicationId:(NSString *)appId userId:(NSString *)userId andSDK_Key:(NSString*)key devEnvironment:(DevelopmentEnvironment)environment withCompletionHandler:(SuccessHandler)success failureBlock:(FailureHandler)failure;
+ (NSString *)getUserId;
+ (NSString *)getAppId;
+ (NSString *)getSDK_key;
+ (DevelopmentEnvironment)getDevEnvironment;
+ (void)logoutUser;
+ (NSString *)getSDKVersion;
@end
