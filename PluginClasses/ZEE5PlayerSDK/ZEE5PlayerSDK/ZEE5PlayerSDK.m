//
//  ZEE5PlayerSDK.m
//  ZEE5PlayerSDK
//
//  Created by admin on 04/12/18.
//  Copyright © 2018 ZEE5. All rights reserved.
//

#import "ZEE5PlayerSDK.h"

static NSString *user_id = @"";
static NSString *app_id = @"";
static NSString *sdk_key = @"";
static DevelopmentEnvironment dev_environment = development;
@implementation ZEE5PlayerSDK

+ (void)initializeWithApplicationId:(NSString *)appId userId:(NSString *)userId andSDK_Key:(NSString*)key devEnvironment:(DevelopmentEnvironment)environment withCompletionHandler:(SuccessHandler)success failureBlock:(FailureHandler)failure
{
    user_id = [Utility makeUserId:userId];
    app_id = appId;
    sdk_key = key;
    dev_environment = environment;
    
    
    NSDictionary *requestParam = @{@"app_id":appId,@"user_id":user_id,@"sdk_key":sdk_key};
    NSDictionary *headers = @{@"Content-Type":@"application/json"};

    
    [[NetworkManager sharedInstance] makeHttpGetRequest:BaseUrls.userAuthorization requestParam:requestParam requestHeaders:headers withCompletionHandler:^(id  _Nullable result) {        
        if ([result isKindOfClass:[NSDictionary class]]) {
            success([NSString stringWithFormat:@"ZEE5 Player SDK Successfully Initilized. \n%@", [self getSDKVersion]]);
            [[AppConfigManager sharedInstance] setConfig:[ZEE5ConfigDataModel initFromJSONDictionary:result]];
            [self getPlateFormToken];
            [self loginRegister];

        }
    } failureBlock:^(ZEE5SdkError * _Nullable error) {
        failure(error);
        NSLog(@"%@", error.message);
    }];
    
}

+ (void)getPlateFormToken
{
    NSMutableDictionary *param = [[NSMutableDictionary alloc] init];
    [param setValue:@"jioSdk" forKey:@"platform_name"];
    NSDictionary *headers = @{@"Content-Type":@"application/json"};
   
    [[NetworkManager sharedInstance] makeHttpGetRequest:BaseUrls.plateFormToken requestParam:param requestHeaders:headers withCompletionHandler:^(id  _Nullable result) {
        NSLog(@"%@", result);
        if ([result isKindOfClass:[NSDictionary class]] && [[result valueForKey:@"token"] isKindOfClass:[NSString class]]) {
            [ZEE5UserDefaults setPlateFormToken:[result valueForKey:@"token"]];
        }


    } failureBlock:^(ZEE5SdkError * _Nullable error) {
        
    }];
    
}

+ (void)loginRegister
{
   
    NSMutableDictionary *param = [[NSMutableDictionary alloc] init];
    [param setValue:@"email" forKey:@"type"];
    [param setValue:user_id forKey:@"value"];
    [param setValue:[AppConfigManager sharedInstance].config.authKey forKey:@"partner_key"];
    [param setValue:[Utility getAdvertisingIdentifier] forKey:@"advertsing_id"];

    NSDictionary *headers = @{@"Content-Type":@"application/json"};
    
    [[NetworkManager sharedInstance] makeHttpRequest:@"POST" requestUrl:BaseUrls.userRegistration requestParam:param requestHeaders:headers withCompletionHandler:^(id  _Nullable result) {
        if ([result isKindOfClass:[NSDictionary class]] && [[result valueForKey:@"token"] isKindOfClass:[NSString class]]) {
            [ZEE5UserDefaults setUserToken:[result valueForKey:@"token"]];
        }
    } failureBlock:^(ZEE5SdkError * _Nullable error) {
        NSLog(@"%@", error.message);
    }];
}

+ (NSString *)getUserId{
    return user_id;
}
+ (NSString *)getAppId{
    return app_id;
}
+ (NSString *)getSDK_key{
    return sdk_key;
}
+ (DevelopmentEnvironment)getDevEnvironment
{
    return dev_environment;
}
+ (void)logoutUser{
    
}
+ (NSString *)getSDKVersion
{
    NSDictionary *infoDictionary = [[NSBundle bundleForClass:self] infoDictionary];
    NSString *majorVersion = [infoDictionary objectForKey:@"CFBundleShortVersionString"];
    NSString *minorVersion = [infoDictionary objectForKey:@"CFBundleVersion"];
    
    return [NSString stringWithFormat:@"ZEE5 Player SDK Version %@ (%@)", majorVersion, minorVersion];
}



@end
