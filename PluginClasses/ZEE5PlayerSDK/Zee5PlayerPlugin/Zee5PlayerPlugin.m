//
//  Zee5PlayerPlugin.m
//  ZEE5PlayerSDK
//
//  Created by Mani on 29/05/19.
//  Copyright © 2019 ZEE5. All rights reserved.
//

#import "Zee5PlayerPlugin.h"
#import <PlayKit/PlayKit-Swift.h>
#import <PlayKit_IMA/PlayKit_IMA-Swift.h>
#import "FormPostFairPlayLicenseProvider.h"
#import "FormRequestParamsAdapter.h"
#import "CurrentItem.h"
#import "ZEE5PlayerManager.h"

static NSString *const kPrerollTag = @"https://pubads.g.doubleclick.net/gampad/ads?iu=%2F21665149170%2FZee5_Web_Preroll&env=vp&impl=s&ad_rule=0&tfcd=0&npa=0&gdfp_req=1&output=vast&sz=640x480&unviewed_position_start=1&cust_params=category%3D%26video_id%3D0-1-8934%26video_title%3DPunar%2BVivaha%2B-%2BEpisode%2B642%2B-%2BSeptember%2B1%26episode_number%3D642%26show_id%3D0-6-38%26season_id%3D0-2-PunarVivaha1%26asset_subtype%3Depisode%26audio_language%3Dkn%26collection_id%3D%26content_creator%3DZee%2BEntertainment%2BEnterprises%2BLtd%26certificate_type%3D%26genre%3DDrama%2CEntertainment%2CRomance%26channel_id%3D%26app_version%3D&description_url=0&is_lat=&correlator=2312213";



@interface Zee5PlayerPlugin()

@property CurrentItem *currentItem;
@property (strong, nonatomic) FormPostFairPlayLicenseProvider *fairplayProvider;
@property (strong, nonatomic) FormRequestParamsAdapter *requestAdapter;

@end


@implementation Zee5PlayerPlugin

static Zee5PlayerPlugin *sharedManager = nil;
+ (Zee5PlayerPlugin *)sharedInstance
{
    if (sharedManager) {
        return sharedManager;
    }
    
    static dispatch_once_t  t = 0;
    
    dispatch_once(&t, ^{
        sharedManager = [[Zee5PlayerPlugin alloc] init];
    });
    
    return sharedManager;
}


-(void)initializePlayer : (PlayerView *)playbackView andItem :(CurrentItem *)currentItem andLicenceURI:(NSString *)licence  andBase64Cerificate:(NSString *)cerificate
{
    self.fairplayProvider = [[FormPostFairPlayLicenseProvider alloc] init];

    self.currentItem = currentItem;
    
    if (self.player) {
        [self.player stop];
    }
    
    
    PluginConfig *pluginConfig = [self createPluginConfig];
    self.player = [[PlayKitManager sharedInstance] loadPlayerWithPluginConfig:pluginConfig];
//    self.player.delegate = self;
    self.player.settings.fairPlayLicenseProvider = self.fairplayProvider;
    


    [self registerPlayerEvents];
    
    self.player.view = playbackView;
    
    
    NSString *contentStringURL;
    if (self.currentItem.isDRM) {
        contentStringURL = self.currentItem.hls_Url;
        
    } else {
        contentStringURL = self.currentItem.hls_Full_Url;
    }
    
    NSURL *contentURL = [[NSURL alloc] initWithString:contentStringURL];
    
    NSString *entryId = self.currentItem.content_id;
    

    FairPlayDRMParams* fairplayParams = [[FairPlayDRMParams alloc] initWithLicenseUri:licence base64EncodedCertificate:cerificate];
    
    
    PKMediaSource* source = [[PKMediaSource alloc] init:entryId contentUrl:contentURL mimeType:nil drmData:@[fairplayParams] mediaFormat:MediaFormatHls];
    
    PKMediaEntry *mediaEntry = [[PKMediaEntry alloc] init:entryId sources:@[source] duration:-1];
    
    // create media config
    MediaConfig *mediaConfig = [[MediaConfig alloc] initWithMediaEntry:mediaEntry startTime:0.0];
    

    self.fairplayProvider.customData = self.currentItem.drm_token;
    
    [self.player prepare:mediaConfig];
    
    [self.player play];
}

- (PluginConfig *)createPluginConfig {
    NSMutableDictionary *pluginConfigDict = [NSMutableDictionary new];
    IMAConfig *imaConfig = [IMAConfig new];
    NSString *vmapString = [self vmapTagBuilder];
    NSLog(@"%@",vmapString);

    imaConfig.adTagUrl = kPrerollTag;
    pluginConfigDict[IMAPlugin.pluginName] = imaConfig;
    return [[PluginConfig alloc] initWithConfig:pluginConfigDict];
}


-(void)initializePlayer : (PlayerView *)playbackView andURL :(NSString *)urlString andToken:(NSString *)token playbacksession_id:(NSString *)playbacksession_id
{
    self.requestAdapter = [[FormRequestParamsAdapter alloc] init];

    if (self.player) {
        [self.player stop];
    }
    PluginConfig *pluginConfig = [self createPluginConfig];

    self.player = [[PlayKitManager sharedInstance] loadPlayerWithPluginConfig:pluginConfig];
    self.player.settings.contentRequestAdapter = self.requestAdapter;

    [self registerPlayerEvents];
    
    self.player.view = playbackView;
    
    
    NSString *contentStringURL = urlString;
    if(![contentStringURL containsString:@"hdnea="] && ![token isEqualToString:@""])
    {
        contentStringURL = [NSString stringWithFormat:@"%@%@",contentStringURL,token];
    }
    
    NSURL *contentURL = [[NSURL alloc] initWithString:contentStringURL];
    
    NSString *entryId = self.currentItem.content_id;
    
    PKMediaSource* source = [[PKMediaSource alloc] init:entryId contentUrl:contentURL mimeType:nil drmData:nil mediaFormat:MediaFormatHls];
    
    PKMediaEntry *mediaEntry = [[PKMediaEntry alloc] init:entryId sources:@[source] duration:-1];
    
    // create media config
    MediaConfig *mediaConfig = [[MediaConfig alloc] initWithMediaEntry:mediaEntry startTime:0.0];
    
    self.requestAdapter.header = @{@"x-playback-session-id": playbacksession_id};
    
    [self.player prepare:mediaConfig];
    [self.player play];
    
}

-(void)registerPlayerEvents
{
    [self registerPlayEvent];
    [self registerDurationChangedEvent];
    [self registerPlayerEventStateChangedEvent];
    [self registerBufferEvent];
    [self registerEndEvent];
    [self registerErrorEvent];
}

// Handle basic event (Play, Pause, CanPlay ..)
- (void)registerPlayEvent {
    
    [self.player addObserver:self
                       event:PlayerEvent.playing
                       block:^(PKEvent * _Nonnull event) {
                           [[ZEE5PlayerManager sharedInstance] onPlaying];
                           NSLog(@"Playing Event");
                       }];
    
    [self.player addObserver:self
                      events:@[PlayerEvent.pause]
                       block:^(PKEvent * _Nonnull event) {
                           [[ZEE5PlayerManager sharedInstance] pause];
                       }];
    
}

// Handle Duration Changes
- (void)registerDurationChangedEvent {
    
    [self.player addObserver:self
                      events:@[PlayerEvent.durationChanged]
                       block:^(PKEvent * _Nonnull event) {
                           if (event.currentTime) {
                               [[ZEE5PlayerManager sharedInstance] onDurationUpdate:event];
                           }
                       }];
    
    
    [self.player addObserver:self
                      events:@[PlayerEvent.playheadUpdate]
                       block:^(PKEvent * _Nonnull event) {
                           if (event.currentTime) {
                               [[ZEE5PlayerManager sharedInstance] onTimeChange:event];
                           }
                       }];
}

// Handle State Changes
- (void)registerPlayerEventStateChangedEvent {
    [self.player addObserver:self
                      events:@[PlayerEvent.stateChanged]
                       block:^(PKEvent * _Nonnull event) {
                           PlayerState oldState = event.oldState;
                           PlayerState newState = event.newState;
                           NSLog(@"State Chnaged Event:: oldState: %d | newState: %d", (int)oldState, (int)newState);
                       }];
}

-(void)registerBufferEvent
{
    
    [self.player addObserver:self
                      events:@[PlayerEvent.playbackStalled]
                       block:^(PKEvent * _Nonnull event) {
                           [[ZEE5PlayerManager sharedInstance] onBuffring];
                           NSLog(@"Buffer");
                       }];
    
    
    [self.player addObserver:self
                      events:@[PlayerEvent.loadedTimeRanges]
                       block:^(PKEvent * _Nonnull event) {
                           [[ZEE5PlayerManager sharedInstance] onBuffringValueChange:event];
                       }];
    
}

-(void)registerEndEvent{
    [self.player addObserver:self
                      events:@[PlayerEvent.ended]
                       block:^(PKEvent * _Nonnull event) {
                           [[ZEE5PlayerManager sharedInstance] onComplete];
                       }];
}
// Handle Player Errors
- (void)registerErrorEvent {
    
    [self.player addObserver:self events:@[PlayerEvent.error] block:^(PKEvent * _Nonnull event) {
        NSError *error = event.error;
        if (error && (error.code == PKErrorCode.AssetNotPlayable || error.code == PKErrorCode.FailedToLoadAssetFromKeys)) {
            
        }
    }];
}


-(NSTimeInterval )getDuration
{
    return self.player.duration;
}
-(NSTimeInterval )getCurrentTime
{
    return self.player.currentTime;
}
-(void)setSeekTime:(NSInteger)value
{
    [self.player seekTo:value];
}

- (BOOL)playerShouldPlayAd:(id<Player>)player {
    return YES;
}


-(NSString *)vmapTagBuilder
{
    NSString *totalString = @"";
    NSString *part1 = @"<vmap:VMAP xmlns:vmap=\"http://www.iab.net/videosuite/vmap\" version=\"1.0\">\n";
    NSString *part3 = @"</vmap:VMAP>";
    NSString *part2 = @"";

    for (ZEE5AdModel *model in self.currentItem.googleAds) {
        
        NSInteger index = [self.currentItem.googleAds indexOfObject:model];
        if ([model.time.lowercaseString isEqualToString:@"pre"]) {
            part2 = [NSString stringWithFormat:@"%@%@%@%@",part2,@"<vmap:AdBreak timeOffset=\"start\" breakType=\"linear\" breakId=\"preroll\">\n<vmap:AdSource id=\"preroll-ad-1\" allowMultipleAds=\"false\" followRedirects=\"true\">\n<vmap:AdTagURI templateType=\"vast3\">\n<![CDATA[\n",model.tag,@"]]>\n</vmap:AdTagURI>\n</vmap:AdSource>\n</vmap:AdBreak>"];
        }
        else if ([model.time.lowercaseString isEqualToString:@"post"]) {
            part2 = [NSString stringWithFormat:@"%@%@%@%@",part2,@"<vmap:AdBreak timeOffset=\"end\" breakType=\"linear\" breakId=\"postroll\">\n<vmap:AdSource id=\"postroll-ad-1\" allowMultipleAds=\"false\" followRedirects=\"true\">\n<vmap:AdTagURI templateType=\"vast3\">\n<![CDATA[\n",model.tag,@"]]>\n</vmap:AdTagURI>\n</vmap:AdSource>\n</vmap:AdBreak>"];
        }
        else
        {
            part2 = [NSString stringWithFormat:@"%@%@%@%@%ld%@%@%@%@%@",part2,@"<vmap:AdBreak timeOffset=\"",model.time,@"\" breakType=\"linear\" breakId=\"midroll-",(long)index,@"\">\n<vmap:AdSource id=\"",model.tag_name,@"\" allowMultipleAds=\"false\" followRedirects=\"true\">\n<vmap:AdTagURI templateType=\"vast3\">\n<![CDATA[\n",model.tag,@"]]>\n</vmap:AdTagURI>\n</vmap:AdSource>\n</vmap:AdBreak>"];

        }
    }
    
    totalString = [NSString stringWithFormat:@"%@%@%@",part1,part2,part3];

    return totalString;
}





@end
