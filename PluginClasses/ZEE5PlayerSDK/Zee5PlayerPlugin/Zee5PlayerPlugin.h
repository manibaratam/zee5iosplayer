//
//  Zee5PlayerPlugin.h
//  ZEE5PlayerSDK
//
//  Created by Mani on 29/05/19.
//  Copyright © 2019 ZEE5. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <PlayKit/PlayKit-Swift.h>
#import "ZEE5AdModel.h"
#import "CurrentItem.h"
NS_ASSUME_NONNULL_BEGIN

@interface Zee5PlayerPlugin : NSObject
+ (Zee5PlayerPlugin *)sharedInstance;
@property (strong, nonatomic) id<Player> player;

-(void)initializePlayer : (PlayerView *)playbackView andItem :(CurrentItem *)currentItem andLicenceURI:(NSString *)licence  andBase64Cerificate:(NSString *)cerificate;
-(void)initializePlayer : (PlayerView *)playbackView andURL :(NSString *)urlString andToken:(NSString *)token playbacksession_id:(NSString *)playbacksession_id;
-(NSTimeInterval )getDuration;
-(NSTimeInterval )getCurrentTime;
-(void)setSeekTime:(NSInteger)value;
@end

NS_ASSUME_NONNULL_END
