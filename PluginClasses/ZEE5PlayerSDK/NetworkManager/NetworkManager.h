//
//  NetworkManager.h
//  ZEE5PlayerSDK
//
//  Created by admin on 04/12/18.
//  Copyright © 2018 ZEE5. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ZEE5UserDefaults.h"
#import "BaseUrls.h"
#import "Utility.h"
#import "ZEE5SdkError.h"
#import "NetworkManager.h"
#import "AppConfigManager.h"
#import "ReportingManager.h"
#import "NSDictionary+Extra.h"
#import "Genres.h"
#import "RelatedVideos.h"
#import "ZEE5PlayerManager.h"

typedef void(^SuccessHandler)(id _Nullable result);
typedef void(^FailureHandler)(ZEE5SdkError * _Nullable error);

NS_ASSUME_NONNULL_BEGIN

@interface NetworkManager : NSObject
+ (NetworkManager *)sharedInstance;
- (void)authenticateWithServer:(NSString *)app_id userId:(NSString *)user_id andSDK_key:(NSString *)key withCompletionHandler:(SuccessHandler)success failureBlock:(FailureHandler)failure;
- (void)makeHttpGetRequest:(NSString *)urlString requestParam:(NSDictionary*)param requestHeaders:(NSDictionary*)headers withCompletionHandler:(SuccessHandler)success failureBlock:(FailureHandler)failure;
- (void)makeHttpRequest:(NSString *)requestname requestUrl:(NSString*)urlString requestParam:(NSDictionary*)param requestHeaders:(NSDictionary*)headers withCompletionHandler:(SuccessHandler)success failureBlock:(FailureHandler)failure;
-(void)getCountry:(void (^)(NSString *))completion failureBlock:(FailureHandler)failure;
-(void)getCountryListWithID:(NSString *)code completion:(void (^)(NSDictionary *))completion failureBlock:(FailureHandler)failure;
-(void)getSettingsWithCompletion:(void (^)(NSDictionary *))completion failureBlock:(FailureHandler)failure;

@end

NS_ASSUME_NONNULL_END
