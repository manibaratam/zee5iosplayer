//
//  ZEE5UserDefaults.m
//  ZEE5PlayerSDK
//
//  Created by admin on 15/12/18.
//  Copyright © 2018 ZEE5. All rights reserved.
//

#import "ZEE5UserDefaults.h"

@implementation ZEE5UserDefaults

+ (void)setUserToken:(NSString*)token
{
    [[NSUserDefaults standardUserDefaults] setValue:token forKey:@"userToken"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}
+ (NSString *)getUserToken
{
    NSString *userToken = [[NSUserDefaults standardUserDefaults] valueForKey:@"userToken"];
    if (userToken) {
        return userToken;
    }
    return @"User token not available";
}
+ (void)setPlateFormToken:(NSString*)token
{
    [[NSUserDefaults standardUserDefaults] setValue:token forKey:@"plateFormToken"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}
+ (NSString *)getPlateFormToken
{
    NSString *userToken = [[NSUserDefaults standardUserDefaults] valueForKey:@"plateFormToken"];
    if (userToken) {
        return userToken;
    }
    return @"Plateform token not available";
}
@end
