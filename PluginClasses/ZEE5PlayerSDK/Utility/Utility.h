//
//  Utility.h
//  ZEE5PlayerSDK
//
//  Created by admin on 11/12/18.
//  Copyright © 2018 ZEE5. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ZEE5PlayerSDK.h"

NS_ASSUME_NONNULL_BEGIN

@interface Utility : NSObject
+ (NSString *)makeUserId:(NSString*)user_id;
+ (NSString*)getAdvertisingIdentifier;
+ (NSString*)getCommaSaperatedGenreList:(NSArray<Genres*>*)genresArray;
+ (NSString *)convertEpochToTime:(NSTimeInterval)time;
+ (NSString *)convertEpochToDateTime:(NSTimeInterval)time;
+ (NSString *)convertEpochToDetailTime:(NSTimeInterval)time;
+ (NSString *)convertFullDateToDate:(NSString *)dateObj;
+ (NSString *)getCurrentDateInTimeStamp;
+ (NSString *)convertToString: (StreamType)type;
+ (NSString *)getLanguageStringFromId : (NSString *)strID;
+ (NSString *)getDuration:(NSInteger )currentDuraton total:(NSInteger)totalDuraton;
+ (NSMutableAttributedString *)addAttribuedFont :(NSTimeInterval)startTime :(NSTimeInterval)endTime;
+ (NSInteger)getSeconds:(NSString *)time;
@end

NS_ASSUME_NONNULL_END
