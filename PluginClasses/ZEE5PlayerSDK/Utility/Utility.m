//
//  Utility.m
//  ZEE5PlayerSDK
//
//  Created by admin on 11/12/18.
//  Copyright © 2018 ZEE5. All rights reserved.
//

#import "Utility.h"
#import <AdSupport/ASIdentifierManager.h>

@implementation Utility
+ (NSString *)makeUserId:(NSString*)user_id
{
    NSString *userId = user_id;
    
    if ([userId length] > 0) {
        
        if ([userId rangeOfString:@"@"].location == NSNotFound) {
            return [NSString stringWithFormat:@"%@@zee5jio.com", userId];
            
        } else {
            return userId;
        }
    }
    else{
        return [NSString stringWithFormat:@"%@@jio.com", [self getAdvertisingIdentifier]];
    }
    
    
    return user_id;
}

+ (NSString*)getAdvertisingIdentifier
{
    NSUUID *adId = [[ASIdentifierManager sharedManager] advertisingIdentifier];
    return [adId UUIDString];
}

+ (NSString*)getCommaSaperatedGenreList:(NSArray<Genres*>*)genresArray
{
    NSString *genreString = @"";
    for (Genres *genre in genresArray) {
        genreString = [NSString stringWithFormat:@"%@,",[genreString stringByAppendingString:genre.value]];
    }
    if ([genreString length] > 0) {
        genreString = [genreString substringToIndex:[genreString length] - 1];
    }
    return genreString;
}
+ (NSString *)convertFullDateToDate:(NSString *)dateObj
{
    
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];
    
    NSDate *date = [format dateFromString:dateObj];
    [format setDateFormat:@"dd MMM"];
    NSString* finalDateString = [format stringFromDate:date];
    
    return finalDateString;
}

+ (NSString *)convertEpochToTime:(NSTimeInterval)time
{
    NSDate* date = [NSDate dateWithTimeIntervalSince1970:time];
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    format.dateFormat = @"hh:mm a";
    
    NSString *strDate = [format stringFromDate:date];
    return strDate;
}

+ (NSString *)convertEpochToDateTime:(NSTimeInterval)time
{
    NSDate* date = [NSDate dateWithTimeIntervalSince1970:time];
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    format.dateFormat = @"EEEE, dd MMM, hh:mm a";
    
    NSString *strDate = [format stringFromDate:date];
    return strDate;
}

+ (NSString *)convertEpochToDetailTime:(NSTimeInterval)time
{
    NSDate* date = [NSDate dateWithTimeIntervalSince1970:time];
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    format.dateFormat = @"hh:mm:ss a";
    
    NSString *strDate = [format stringFromDate:date];
    return strDate;
}

+ (NSString *)getCurrentDateInTimeStamp
{
    NSTimeInterval timeStamp = [[NSDate date] timeIntervalSince1970];
    NSNumber *timeStampObj = [NSNumber numberWithDouble: timeStamp];
    return [NSString stringWithFormat:@"%ld",(long)[timeStampObj integerValue]];
}

+ (NSString *)convertToString: (StreamType)type
{
    switch (type) {
        case vod:
            return @"vod";
            break;
        case live:
            return @"live";
            break;
        case dvr:
            return @"dvr";
            break;
            
        default:
            return @"";
            
            break;
    }
}
+ (NSString *)getLanguageStringFromId : (NSString *)strID
{
    if ([strID isEqualToString:@"en"]) {
        return @"English";
    }
    else if ([strID isEqualToString:@"hi"]) {
        return @"Hindi";
    }
    else if ([strID isEqualToString:@"mr"]) {
        return @"Marathi";
    }
    else if ([strID isEqualToString:@"te"]) {
        return @"Telugu";
    }
    else if ([strID isEqualToString:@"kn"]) {
        return @"Kannada";
    }
    else if ([strID isEqualToString:@"ta"]) {
        return @"Tamil";
    }
    else if ([strID isEqualToString:@"ml"]) {
        return @"Malayalam";
    }
    else if ([strID isEqualToString:@"bn"]) {
        return @"Bengali";
    }
    else if ([strID isEqualToString:@"gu"]) {
        return @"Gujarati";
    }
    else if ([strID isEqualToString:@"pa"]) {
        return @"Punjabi";
    }
    else if ([strID isEqualToString:@"hr"]) {
        return @"Bhojpuri";
    }
    else if ([strID isEqualToString:@"or"]) {
        return @"Oriya";
    }
    return strID;
}
+ (NSString *)getDuration:(NSInteger )currentDuraton total:(NSInteger)totalDuraton
{
    int seconds = currentDuraton % 60;
    int minutes = (currentDuraton / 60) % 60;
    int hours = currentDuraton / 3600;
    
    //    int totalHours = totalDuraton / 3600;
    
    if(hours == 0)
    {
        return [NSString stringWithFormat:@"%02d:%02d", minutes, seconds];
    }
    else
    {
        return [NSString stringWithFormat:@"%02d:%02d:%02d",hours, minutes, seconds];
        
    }
    
}

+ (NSInteger)getSeconds:(NSString *)time
{
    NSArray *components = [time componentsSeparatedByString:@":"];
    
    NSInteger hours   = [[components objectAtIndex:0] integerValue];
    NSInteger minutes = [[components objectAtIndex:1] integerValue];
    NSInteger seconds = [[components objectAtIndex:2] integerValue];
    
    return (hours * 60 * 60) + (minutes * 60) + seconds;
    
}


+(NSMutableAttributedString *)addAttribuedFont :(NSTimeInterval)startTime :(NSTimeInterval)endTime
{
    NSDictionary *arialDict1 = [NSDictionary dictionaryWithObject: [UIColor whiteColor] forKey:NSForegroundColorAttributeName];
    NSMutableAttributedString *aAttrString1 = [[NSMutableAttributedString alloc] initWithString:@"Playing '" attributes: arialDict1];
    
    NSDictionary *arialDict2 = [NSDictionary dictionaryWithObject: [UIColor colorWithRed:234.0/255.0 green:114.0/255.0 blue:67.0/255.0 alpha:1.0] forKey:NSForegroundColorAttributeName];
    NSMutableAttributedString *aAttrString2 = [[NSMutableAttributedString alloc] initWithString:@"CATCHUP" attributes: arialDict2];
    
    NSMutableAttributedString *aAttrString3 = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"' from %@ - %@",[Utility convertEpochToDateTime:startTime],[Utility convertEpochToTime:endTime]] attributes: arialDict1];
    
    [aAttrString1 appendAttributedString:aAttrString2];
    [aAttrString1 appendAttributedString:aAttrString3];
    return aAttrString1;
    
    
}

@end
