//
//  ZEE5UserDefaults.h
//  ZEE5PlayerSDK
//
//  Created by admin on 15/12/18.
//  Copyright © 2018 ZEE5. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface ZEE5UserDefaults : NSObject
+ (void)setUserToken:(NSString*)token;
+ (NSString *)getUserToken;
+ (void)setPlateFormToken:(NSString*)token;
+ (NSString *)getPlateFormToken;
@end

NS_ASSUME_NONNULL_END
