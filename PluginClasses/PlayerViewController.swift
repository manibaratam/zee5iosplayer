//
//  ViewController.swift
//  KalturaTestApp
//
//  Created by Mani on 17/04/19.
//  Copyright © 2019 company. All rights reserved.
//

import UIKit

//import ZEE5PlayerSDK
import PlayKit
class PlayerViewController: UIViewController {
    
    @IBOutlet weak var playerView: PlayerView!

    override func viewDidLoad() {
        super.viewDidLoad()
        
//        san masvdsad
        ZEE5PlayerManager.sharedInstance().delegate = self
        let config = ZEE5PlayerConfig()
        
        //        let currentTime = TimeInterval.init(Int64(Date().timeIntervalSince1970) - 2400)
        //        let endTime = TimeInterval.init(Int64(Date().timeIntervalSince1970) - 600)
        
        ZEE5PlayerManager.sharedInstance().playLiveContent("", showId: "CHN-000050070PRG-550504662", startTime: 1551209400, endtime: 1551211200, episodNumber: "1", seasonNumber: "1", category: "", translation: "en", playerConfig: config, playbackView: playerView)
        
        NotificationCenter.default.addObserver(self, selector: #selector(videoDidRotate), name: UIDevice.orientationDidChangeNotification, object: nil)
        
        
        // Do any additional setup after loading the view, typically from a nib.
    }
    @objc func videoDidRotate() {
        
        if UIApplication.shared.statusBarOrientation.isLandscape {
            self.navigationController?.setNavigationBarHidden(true, animated: true)
        }
        else{
            self.navigationController?.setNavigationBarHidden(false, animated: true)
            
        }
    }
}

extension PlayerViewController : ZEE5PlayerDelegate
{
    
    func didFinishPlaying() {
        
    }
    func didTaponLiveButton(_ str: String) {
        print(str)
    }
    func didTaponLockButton(_ state: PlayerControlState) {
        if state == PlayerControlState.Locked
        {
            //AppUtility.lockOrientation(.landscape)
        }
        else
        {
            //AppUtility.lockOrientation(.all)
        }
    }
    func didTaponMinimizeButton() {
        
    }
    func didTaponNextButton() {
        
    }
    func didTaponPrevButton() {
        
    }
    func playerData(_ dict: [AnyHashable : Any]) {
    }
    
    
}
